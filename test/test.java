
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Arrays;

import ers.Issue;
import ers.Severity;
import ers.Severity.level;


public class test {

    ers.Issue Message;

    static class TestIssue extends ers.Issue {
        private static final long serialVersionUID = -7192802618905197833L;

        TestIssue(String message) {
            super(message);
        }
    }

    public static boolean ERS_RANGE_CHECK(int min, int val, int max) {
        return(((min) < (val) && (val) < (max)) ? true : false);
    }

    /*
     * *********************************************************** ISSUE TESTINGS
     **************************************************************/

    /**
     * this function tests the setting and getting of a severity in an issue
     * 
     * @return true if everything goes well
     */
    static boolean testSeveritySetting() {

        Issue issue = new Issue("test program");
        Severity s = new Severity(level.Fatal);
        issue.setSeverity(s);
        if(issue.severity().level() != level.Fatal)
            return false;
        return true;

    }

    /**
     * this function tests the setting and getting of a value in an issue (value correspond to the parameters of the issue)
     * 
     * @return true if everything goes well
     */
    static boolean testValueSetting() {
        Issue issue = new Issue("test program");
        issue.setValue("1", "val1");
        issue.setValue("2", "val2");
        issue.setValue("3", "val3");

        final String sb = issue.parameters().get("3");
        return sb.equals("3");
    }

    static boolean testParameters() {
        Issue issue = new Issue("test program");
        issue.setValue("1", "val1");
        issue.setValue("2", "val2");
        issue.setValue("3", "val3");

        String tempVal[] = new String[3];
        Arrays.fill(tempVal, "3 = val3"); // I begin by the last key/value because this is the first in the map
        Arrays.fill(tempVal, "2 = val2");
        Arrays.fill(tempVal, "1 = val1");

        String temp[] = new String[issue.parameters().size()];
        for(Iterator<Entry<String, String>> iterator = issue.parameters().entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<String, String> entry = iterator.next();
            Arrays.fill(temp, entry.getKey() + " = " + entry.getValue());
        }

        if(!Arrays.equals(tempVal, temp))
            return false;

        return true;
    }

    /*
     * static boolean testStreamManager(){ Issue issue = new Issue(ersSystem.ERS_HERE(),"test stream manager"); FileOutStreamTest fosT = new
     * FileOutStreamTest(); MRSOutStream MRSstream = new MRSOutStream();
     * System.out.println("\n################################################\n"); StreamManager.registerStream("fosT", fosT);
     * StreamManager.instance().add_output_stream(ers.severity.Information, fosT); StreamManager.registerStream("MRS", MRSstream);
     * StreamManager.instance().add_output_stream(ers.severity.Information, MRSstream);
     * System.out.println(StreamManager.instance().get_stream(ers.severity.Information).toString());
     * StreamManager.instance().information(issue); ersSystem.debug(issue,0);
     * System.out.println("\n################################################\n"); ersSystem.info(issue); return true; }
     */

    static boolean testIssueConstructorMessage() {
        Issue issue = new Issue("test constructor Issue(message)");
        ers.Logger.error(issue);
        return true;
    }

    public static void main(String[] args) throws Exception {
        Issue issue = new Issue("test program");
        TestIssue tissue = new TestIssue("MyTestIssue");
        tissue.addQualifier("Q1: first qualifier");

        if(!testSeveritySetting())
            System.exit(10);

        if(!testValueSetting())
            System.exit(20);

        if(!testParameters())
            System.exit(50);

        // testStreamManager();

        // if (!testIssueConstructorMessage())
        // System.exit(70);

        System.out.println("\nTDAQ_ERS_ERROR: " + System.getenv("TDAQ_ERS_ERROR"));
        System.out.println("\nTDAQ_ERS_DEBUG: " + System.getenv("TDAQ_ERS_DEBUG"));
        System.out.println("\nTDAQ_ERS_INFO: " + System.getenv("TDAQ_ERS_INFO"));

        ers.Logger.error(tissue);
        ers.Logger.debug(0, issue);
        ers.Logger.debug(1, issue);
        ers.Logger.fatal(issue);
        ers.Logger.log(issue);
        System.exit(0);

    }

}
