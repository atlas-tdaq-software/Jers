import ers.Issue;


public class TestStreams {

    // a class without parameters
    static class TestIssue extends ers.Issue {
        private static final long serialVersionUID = 1L;

        TestIssue(String message) {
            super(message);
        }

        TestIssue(String message, Issue cause) {
            super(message, cause);
        }
    }

    static void my_test_function() {
        ers.Logger.debug(1, new TestIssue("TestIssue3 test issue"));
        ers.Logger.log("TestIssue3 test message");

        String param1 = "serious shit happened";
        int param2 = 19;
        ers.Logger.error(new MyTestIssue(param1, param2, new TestIssue("A funny cause")));
        ers.Logger.error(new MyTestIssue("another great shit on another port", 23));
    }

    public static void main(String[] args) throws Exception {

        Issue issue = new Issue("test program");
        Issue issue2 = new TestIssue("TestIssue test program");

        Issue issue_verbose_1 = new Issue("more verbose message, you will see it");
        Issue issue_verbose_3 = new Issue("very verbose message, you wont see it!");
        Issue error = new Issue("an error occured");
        Issue cause2 = new Issue("more underlying reason");
        Issue cause = new Issue("an underlying reason", cause2);

        TestIssue message = new TestIssue("a specific TestIssue message with cause", cause);
        TestIssue message2 = new TestIssue("a specific TestIssue message without a cause");

        message.addQualifier("TEST_QUAL");
        message.setValue("TestValueString", "Test");
        message.setValue("TestValueInt", 31425);

        java.lang.System.out.println("\nTDAQ_ERS_ERROR: " + java.lang.System.getenv("TDAQ_ERS_ERROR"));
        java.lang.System.out.println("\nTDAQ_ERS_DEBUG: " + java.lang.System.getenv("TDAQ_ERS_DEBUG"));
        java.lang.System.out.println("\nTDAQ_ERS_INFO: " + java.lang.System.getenv("TDAQ_ERS_INFO"));
        java.lang.System.out.println("\nTDAQ_ERS_LOG: " + java.lang.System.getenv("TDAQ_ERS_LOG"));
        java.lang.System.out.println("\nTDAQ_ERS_FATALG: " + java.lang.System.getenv("TDAQ_ERS_FATAL"));

        ers.Logger.debug(0, issue);
        ers.Logger.debug(0, issue2);
        ers.Logger.debug(0, "In-place issue");
        ers.Logger.log("In-place Log message");

        my_test_function();

        ers.Logger.debug(3, issue_verbose_3);
        ers.Logger.debug(2, issue_verbose_1);
        ers.Logger.debug(4, "Very very verbose message");
        java.lang.System.out.println("Severity of issue sent to WARNING is " + cause2.severity());
        ers.Logger.warning(cause2);
        ers.Logger.error(error);
        ers.Logger.info(message);
        ers.Logger.info("Information message not important");
        ers.Logger.log("Log message, not to be archived, just for stdout");
        ers.Logger.error(message);
        ers.Logger.error(message2);
        ers.Logger.fatal(cause);

    }

}
