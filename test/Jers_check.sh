#/usr/bin/env bash
pwd 
echo "************************************************************"
echo "*"
echo "* CHECK Jers functionallity"
echo "*"
echo "************************************************************"
echo 
	
#$TDAQ_JAVA_HOME/../bin/javac -cp $TDAQ_CLASSPATH:$CLASSPATH -d . ../../Jers/examples/TestReporter.java ../../Jers/examples/MyTestIssue.java || { echo "Failed to compile example code"; exit 1 ; }
#echo "Compile OK"

export TDAQ_PARTITION="initial"

export TDAQ_ERS_ERROR="lstdout,filter(Test,e),lstderr"
export TDAQ_ERS_WARNING="lstdout"
export TDAQ_ERS_DEBUG="file(issues.txt)"
export TDAQ_ERS_FATAL="abort"

export TDAQ_ERS_DEBUG_LEVEL="2"
#export TDAQ_ERS_LOG="lstdout"

echo "classpath: .:${TDAQ_CLASSPATH}:${CLASSPATH}"

echo "Test with default verbosity"
export TDAQ_ERS_VERBOSITY_LEVEL="0"
$TDAQ_JAVA_HOME/bin/java -cp testj.jar:ers.jar:${TDAQ_CLASSPATH} examples.TestReporter || { echo "Failed to start examples.TestReporter: " ; exit 1 ;}

echo "Test with verbosity 5, and no LOG & INFO messages"
export TDAQ_ERS_VERBOSITY_LEVEL="5"
export TDAQ_ERS_LOG="null"
export TDAQ_ERS_INFO="null"
export TDAQ_ERS_QUALIFIERS="TESTJERS"
$TDAQ_JAVA_HOME/bin/java -cp testj.jar:ers.jar:${TDAQ_CLASSPATH} examples.TestReporter  || { echo "Failed to start examples.TestReporter: "; exit 1 ;}

echo "Now you'll see messages addressed to DEBUG stream in the issues.txt file:"
cat issues.txt
rm -f issues.txt
