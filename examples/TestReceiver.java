package examples;

import java.util.List;
import java.util.ArrayList;

import ers.StreamManager;
import ers.ReceiverNotFound;


class TestReceiver {

    static class MyIssueReceiver implements ers.IssueReceiver {

        public void receive(ers.Issue issue) {
            // System.err.println("MyIssueReceiver: Issue received");

            // return ;
            try {
                ers.IssuePrinter.println(java.lang.System.out, issue, issue.severity(), ers.Configuration.instance().verbosity_level());
            }
            catch(java.io.IOException ex) {
            }
        }
    }

    public static void main(String argv[]) {

        final int size = 10;

        List<MyIssueReceiver> receivers = new ArrayList<MyIssueReceiver>();

        while(true) {

            try {
                for(int i = 0; i < size; i++) {
                    MyIssueReceiver rec = new MyIssueReceiver();
                    // StreamManager.instance().add_receiver(rec, "mts", "app=XYZ") ;
                    // StreamManager.instance().add_receiver(rec, "mts", "sev=WARNING") ;
                    StreamManager.instance().add_receiver(rec, "mts", "sev=ERROR or qual=A" + i);
                    // StreamManager.instance().add_receiver(rec, "mts", "initial", "*") ; // 2 parameters passed to the srteam constructor
                    // StreamManager.instance().add_receiver(rec, "mts", "initial", "*") ; // 2 parameters passed to the srteam constructor
                    receivers.add(rec);
                }
                // System.err.println("Receivers added");
                Thread.currentThread().sleep(10); // block program, wait for ^C
            }
            catch(ers.BadStreamConfiguration ex) {
                System.err.println("Failed to add a receiver: " + ex.getMessage());
                System.err.println("Cause: " + ex.getCause().getMessage());
                System.exit(0);
            }
            catch(java.lang.InterruptedException ex) {
                System.err.println("Got " + ex.getMessage());
                // System.exit(0) ;
            }

            // System.err.println("Removing receivers");
            for(int i = 0; i < size; i++) {
                try {
                    StreamManager.instance().remove_receiver(receivers.get(i));
                    // System.err.println("Receiver " + receivers.get(i) + " removed") ;
                }
                catch(final ReceiverNotFound ex) {
                    System.err.println("Failed to remove Receiver " + receivers.get(i) + ": " + ex);
                    System.exit(0);
                }
            }
            receivers.clear();

            // System.err.println("Removed receivers");
        }
    }
}
