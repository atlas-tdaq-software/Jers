package examples;

import ers.Issue;

import org.apache.log4j.LogManager;
import org.apache.log4j.Level;
import org.apache.log4j.xml.DOMConfigurator;


public class TestReporter {
    // a class without parameters, just to have a dedicated message ID == "TestIssue"
    // for a message with parameters, see examples/MyTestIssue.java
    // (each public Issue class must be in a separate java source file)
    static class TestIssue extends ers.Issue {
        private static final long serialVersionUID = -6820272876742566515L;

        TestIssue(String message) {
            super(message);
        }

        TestIssue(String message, Issue cause) {
            super(message, cause);
        }
    }

    static class BaseIssue extends Exception {
        private static final long serialVersionUID = -5065254472474794980L;
        final static String msg = "Base Issue happened";

        BaseIssue() {
            super(msg);
        }

        BaseIssue(Exception cause) {
            super(msg, cause);
        }
    }

    static void my_test_function() {

        ers.Logger.debug(1, new TestIssue("TestIssue3 test issue"));
        ers.Logger.log("TestIssue3 test message");

        String param1 = "serious shit happened";
        int param2 = 19;
        ers.Logger.error(new MyTestIssue(param1, param2, new TestIssue("A funny cause")));
        ers.Logger.error(new MyTestIssue("another great shit on another port", 23));
        ers.Logger.error(new MyTestIssue("another great shit on another port", 23));
    }

    static void exception_test_function() throws TestIssue {
        TestIssue message_c = new TestIssue("a specific cause TestIssue to be thrown");
        TestIssue message = new TestIssue("a specific cause TestIssue to be thrown", message_c);
        throw message;
    }

    private static org.apache.log4j.Logger log4jLogger = LogManager.getLogger("TestReporterLogger");

    public static void main(String[] args) throws Exception {

        DOMConfigurator.configure("../examples/log4j_xmlConfig.xml");

        log4jLogger.setLevel(Level.ALL);

        Issue issue = new Issue("test program");

        Issue issue2 = new TestIssue("TestIssue test program");

        Issue issue_verbose_1 = new Issue("more verbose message, you will see it");
        Issue issue_verbose_3 = new Issue("very verbose message, you wont see it!");
        Issue error = new Issue("an error occured");
        Issue cause2 = new Issue("more underlying reason");
        Issue cause = new Issue("an underlying reason", cause2);

        TestIssue message = new TestIssue("a specific TestIssue message with cause, qualifier and parameters", cause);
        TestIssue message2 = new TestIssue("a specific TestIssue message without a cause");

        message.addQualifier("TEST_QUAL");
        message.setValue("TestValueString", "Test");
        message.setValue("TestValueInt", 31425);

        System.out.println("TDAQ_ERS_FATAL: " + System.getenv("TDAQ_ERS_FATAL"));
        System.out.println("TDAQ_ERS_ERROR: " + System.getenv("TDAQ_ERS_ERROR"));
        System.out.println("TDAQ_ERS_WARNING: " + System.getenv("TDAQ_ERS_WARNING"));
        System.out.println("TDAQ_ERS_DEBUG: " + System.getenv("TDAQ_ERS_DEBUG"));
        System.out.println("TDAQ_ERS_INFO: " + System.getenv("TDAQ_ERS_INFO"));
        System.out.println("TDAQ_ERS_LOG: " + System.getenv("TDAQ_ERS_LOG"));

        // System.out.println("\n------------ log4j start ------------------\n");
        // log4jLogger.error(error); // This is an ers Issue reported via log4j
        // log4jLogger.error("This is a log4j error message reported via log4j");

        try {
            double e = 1 / 0;
        }
        catch(final RuntimeException ex) {
            System.out.println("\nReporting Java RuntimeException to WARNING stream:");
            ers.Logger.warning(ex);
            System.out.println("\nReporting Java RuntimeException as nested issue of BaseIssue to WARNING stream:");
            ers.Logger.warning(new BaseIssue(ex));
            System.out.println("\nReporting Java RuntimeException as cause of new Exception to WARNING stream:");
            ers.Logger.error(new Exception(ex));
            System.out.println("\nReporting Java RuntimeException log4j ERROR stream:");
            log4jLogger.error(ex);
        }

        try {
            Object nll = null;
            int h = nll.hashCode();
        }
        catch(final RuntimeException ex) {
            System.out.println("\nReporting NullPointer to WARNING stream:");
            ers.Logger.warning(ex);
            System.out.println("\nReporting NullPointer as nested issue of BaseIssue to WARNING stream:");
            ers.Logger.warning(new BaseIssue(ex));
        }

        // System.out.println("\n-------------- log4j end ------------------\n");

        ers.Logger.debug(0, issue);
        ers.Logger.debug(0, issue2);
        ers.Logger.debug(0, "In-place issue");
        ers.Logger.log("In-place Log message");

        my_test_function();

        try {
            exception_test_function();
        }
        catch(final Issue ex) {
            ers.Logger.error(ex);
        }

        ers.Logger.debug(0, "0 debug message");
        ers.Logger.debug(1, "1 debug message!");
        ers.Logger.debug(2, "2 debug message!!");
        ers.Logger.debug(3, "3 debug message!!!");
        ers.Logger.debug(3, issue_verbose_3);
        ers.Logger.debug(2, issue_verbose_1);
        ers.Logger.debug(4, "Very very verbose message");
        System.out.println("Severity of issue sent to WARNING is " + cause2.severity());
        ers.Logger.warning(cause2);
        ers.Logger.error(error);
        ers.Logger.info(message);
        ers.Logger.info("Information message not important");
        ers.Logger.log("Log message, not to be archived, just for stdout");
        ers.Logger.error(message);
        ers.Logger.error(message2);
        // System.out.println("\n------------ check start ------------------\n");
        // ers.Logger.error(cause);
        // System.out.println("\n------------ check end ------------------\n");
    }
}
