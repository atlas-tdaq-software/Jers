package examples;

import ers.Issue;


// a custom issue with parameters and formatted message
public class MyTestIssue extends ers.Issue {
    private static final long serialVersionUID = 5760461508885940883L;
    public String reason;
    public int port_number;

    MyTestIssue(String reason, int port_number, Issue cause) {
        super("MyTestIssue happened because of %s on a port number %d", reason, port_number, cause);
    }

    MyTestIssue(String reason, int port_number) {
        super("MyTestIssue happened because of %s on a port number %d", reason, port_number);
    }

    MyTestIssue(String reason) {
        super("MyTestIssue happened because of %s on a port number XX", reason);
    }
};
