package ers;

import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import ers.Severity.level;
import ers.streams.StandardStreamOutput;
import ers.streams.NullStream;

import org.reflections.Reflections ;

/**
 * Uses org.reflections.Reflections in static section to get all classes inherited from ers.StreamBase and
 * annotated with ErsStreamName(name='') annotation and to register them. Then uses StreamFactory to instantiate configured
 * (in environment) streams.
 */
public class StreamManager
{
    private static volatile StreamManager INSTANCE = null ;

    private static List<AbstractInputStream> m_in_streams;
    private transient AbstractOutputStream[] m_out_streams;

    static class ShutdownThread extends Thread
    {
	ShutdownThread() { super() ; }

	public void run()
	    {
	    synchronized (m_in_streams) {
	    	for (AbstractInputStream is : m_in_streams)
			{
			if (is != null) { is.cleanUp(); }
			}
		}
	    }
    }

    static
	{ 
		// ers and mts packages are the ones providing ERS streams
		// TODO configuration?
 	org.reflections.Reflections reflections = new org.reflections.Reflections("ers", "mts") ;
	Set<Class<? extends StreamBase>> streamclasses = reflections.getSubTypesOf(StreamBase.class) ;
	
	if ( streamclasses.isEmpty() ) {
	    java.lang.System.err.println("Did not find any streams with Reflections :(") ;
	}
	
	for ( Class<? extends StreamBase> astreamclass: streamclasses ) {
		if ( astreamclass.isAnnotationPresent(ers.ErsStreamName.class) ) {
			java.lang.System.err.println("Found ERS stream " + astreamclass.getName() ) ;
			try {
				StreamFactory.instance().registerStream(astreamclass);
			}
			catch ( BadStream ex ) {
				System.err.println("ers.StreamManager: loading of stream "
				+ astreamclass.getName() + " implementation failed: " + ex.getMessage() ) ;
			continue ; // TODO badly configured stream is FATAL or not ??
			// java.lang.System.exit(1) ; // FATAL or not ??
			}
		}
	} 

/*
	final ServiceLoader<StreamRegistrator> stream_loader = ServiceLoader
	        .load(StreamRegistrator.class);

	for ( StreamRegistrator astream : stream_loader )
	    {
	    // System.err.println("Found stream " + astream.getClass().getName() + " for " + astream.getStreamName()) ;
	    try
		{
		StreamFactory.instance().registerStream(astream.getClass(),
		        astream.getStreamName());
		}
	    catch (BadStream ex)
		{
		System.err.println("ers.StreamManager: loading of stream "
		+ astream.getClass().getName() + " implementation failed: " +
		ex.getMessage() ) ;
		continue;
		// java.lang.System.exit(1) ;
		}
	    }
 */
	try
	    {
	    Runtime.getRuntime().addShutdownHook(new ShutdownThread());
	    }
	catch (Exception ex)
	    {
	    java.lang.System.err.println("Failed to add ShutdownHook: "
		    + ex.getMessage());
	    }
	}

    /** Default streams for severities, corresponds to Severity.level enum
     * 
     */
    public static String DefaultOutputStreams[] = { "lstdout", // Debug
	    "lstdout", // Log
	    "lstdout", // Information
	    "lstderr", // Warning
	    "lstderr", // Error
	    "lstderr" // Fatal
    };

    // singleton pattern
    public static StreamManager instance()
	{
	StreamManager localInstance = INSTANCE ;
	if ( localInstance == null ) {
	    synchronized (StreamManager.class) {
	    	localInstance = INSTANCE ;
	    	if ( localInstance == null )
	    	    {
	    	    localInstance = INSTANCE = new StreamManager() ;
	    	    }
	    }
	}

	return localInstance;
	}

    private StreamManager()
	{
	m_out_streams = new AbstractOutputStream[Severity.level.values().length];
	m_in_streams = new ArrayList<AbstractInputStream>();
	int i = 0;
	for (Severity.level ss : Severity.level.values())
	    {
	    // System.err.println("Setting up streams for severiry " + ss ) ;
	    try
		{
		m_out_streams[i++] = setup_severity_stream(ss);
		}
	    catch (BadStreamConfiguration ex)
		{
		java.lang.System.err
		        .println("ers.StreamManager construction: failed to setup streams for severity "
		                + ss + ":\n" + ex.getMessage());
		continue;
		}
	    }
	}

    public void debug(final Exception issue, final int debug_level)
	{
	report_issue(new Severity(level.Debug, debug_level), issue);
	}

    public void error(final Exception issue)
	{
	report_issue(new Severity(level.Error), issue);
	}

    public void fatal(final Exception issue)
	{
	report_issue(new Severity(level.Fatal), issue);
	}

    public void information(final Exception issue)
	{
	report_issue(new Severity(level.Information), issue);
	}

    public void warning(final Exception issue)
	{
	report_issue(new Severity(level.Warning), issue);
	}

    public void log(final Exception issue)
	{
	report_issue(new Severity(level.Log), issue);
	}
    
    /**
     * New way of adding receiver with variadic number of String parameters in constructor
     * Allows adding receivers with 2 parameters, e.g. partition name and subscription criteria.
     * 
     * StreamFactory instantiates an instance of input stream and assigns user receiver to it.
     * 
     * Examples:
     * 	StreamManager.instance().add_receiver(receiver, "mts", "sev=ERROR or qual=Test") ;	// reads TDAQ_PARTITION environment
     * 	StreamManager.instance().add_receiver(receiver, "mts", "initial", "*") ; 		// partition name is passed explicitly
     * @param receiver ref. to user's Receiver
     * @param stream stream name (e.g. 'mts')
     * @param params a list of parameters for the stream constructor, e.g. partition_name and selection_criteria 
     * @throws BadStreamConfiguration
     */
    public void add_receiver(final IssueReceiver receiver, final String stream, final Object... params) throws BadStreamConfiguration
	{
	final AbstractInputStream instream = StreamFactory.instance().createInStream(stream, params);
	instream.setReceiver(receiver);
	synchronized (m_in_streams) { 
	m_in_streams.add(instream);
	}
	}

    /**
     * Destroys the receiver and removes it from the list.
     * To be called by users willing to unsubscribe from messages.
     *  
     * @param receiver
     * @throws ReceiverNotFound
     */
	public void remove_receiver(final IssueReceiver receiver) throws ReceiverNotFound {
		boolean removed = false;

		synchronized (m_in_streams) {
		Iterator<AbstractInputStream> it = m_in_streams.iterator();
		AbstractInputStream istream;
		while (it.hasNext()) {
			istream = it.next();
			if ( istream.getReceiver() == receiver ) {
				istream.cleanUp();
				it.remove();
				removed = true;
			}
		}
//		java.lang.System.err.println("#of receivers: " + m_in_streams.size()) ;
		} // critical section

		if (!removed) {
			throw new ReceiverNotFound(receiver) ;
		}
	}

    
    // for compatibility
    public void add_receiver(final String stream, final String filter, final IssueReceiver receiver) throws BadStreamConfiguration
 	{
 	add_receiver(receiver, stream, filter) ;
 	}

    // adds new out stream to chain of streams for a severity
    public void add_output_stream(Severity.level severity, AbstractOutputStream new_stream)
	{
	AbstractOutputStream parent = m_out_streams[severity.ordinal()];

	if (parent != null && !parent.isNull())
	    {
	    for (AbstractOutputStream next = parent.chained(); !next.isNull(); parent = next, next = parent.chained())
		{
		parent.chained(new_stream);
		}
	    }
	else
	    {
	    m_out_streams[severity.ordinal()] = new_stream;
	    }
	}

    public void configure_all_output_stream(final Map<String, String> config)
	{
	for (Severity.level sl : Severity.level.values())
	    {
	    m_out_streams[sl.ordinal()].configureStreamChain(config);
	    }
	}

    private void report_issue(final Severity severity, final Exception ex)
	{
	try
	    {
	    // java.lang.System.out.println("Writing issue for severity " + sev_level.ordinal() + " to stream " +
	    // m_out_streams[sev_level.ordinal()]) ;
	    m_out_streams[severity.toInt()].write(ex, severity);
	    }
	catch (final Exception exx)
	    {
	    System.err.println("\nFailed to write issue '"
		    + ex.getMessage() + "' to stream "
		    + m_out_streams[severity.toInt()] + ": " + exx);
	    final AbstractOutputStream t_str = new StandardStreamOutput();
	    t_str.write(ex, severity) ;
	    }
	}

    private AbstractOutputStream setup_severity_stream(level ss)
	    throws BadStreamConfiguration
	{
	final List<String> streams_names = new ArrayList<String>();

	try
	    {
	    // System.err.println("Stream configuration for severity " + ss + ": " + get_stream_description(ss)) ;
	    parse_stream_definition(get_stream_description(ss), streams_names);
	    }
	catch (BadStreamConfiguration ex)
	    {
	    System.err.println("Configuration for the \"" + ss
		            + "\" stream is invalid. Default configuration will be used.");
	    streams_names.clear();
	    try
		{
		parse_stream_definition(DefaultOutputStreams[ss.ordinal()],streams_names);
		}
	    catch (BadStreamConfiguration ex1)
		{
		System.err.println("Default configuration "
		        + DefaultOutputStreams[ss.ordinal()] + " for the \""
		        + ss + "\" stream is invalid. No stream configured.");
		return new NullStream();
		}
	    }

	final AbstractOutputStream main = setup_stream(streams_names);

	if ( main == null ) { return new NullStream(); }
	return main;
	}

    // returns "lstderr,filter(xxx),lstdout" for a severity from TDAQ_ERS_*
    // environment
    public static String get_stream_description(level sev)
	{
	// assert (Severity.Debug.ordinal() <= sev.ordinal() && sev.ordinal() <=
	// Severity.Fatal.ordinal());

	String sev_string = sev.toString().toUpperCase();
	if ("INFORMATION".equals(sev_string))
	    {
	    sev_string = "INFO";
	    }

	if ("DEBUG0".equals(sev_string))
	    {
	    sev_string = "DEBUG";
	    }

	final StringBuffer env_name = new StringBuffer("TDAQ_ERS_");
	env_name.append(sev_string);
	final String env = java.lang.System.getenv(env_name.toString());

	return (env != null) && !env.isEmpty() ? env : DefaultOutputStreams[sev.ordinal()];
	}

    // decodes "lstderr,filter(xxx),lstdout" into atoms
    private void parse_stream_definition(String text, List<String> result)
	    throws BadStreamConfiguration
	{
	int start_p = 0, end_p = 0;
	int brackets_open = 0;

	while (end_p < text.length())
	    {
	    switch (text.charAt(end_p))
	    {
	    case '(':
		++brackets_open;
	    break;
	    case ')':
		--brackets_open;
	    break;
	    case ',':
		if (brackets_open == 0)
		    {
		    result.add(text.substring(start_p, end_p));
		    start_p = end_p + 1;
		    }
	    default:
	    break;

	    }
	    end_p++;
	    }

	if (brackets_open != 0){ throw new BadStreamConfiguration(text); }
	if (start_p != end_p)
	    {
	    result.add(text.substring(start_p, end_p));
	    }
	}

    // build up chained output stream from e.g,
    // TDAQ_ERS_INFO="lstderr,filter(abc),lstdout"
    private AbstractOutputStream setup_stream(List<String> streams)
	    throws BadStreamConfiguration
	{
	// java.lang.System.err.println("Setting up streams " + streams );
	int cnt = 0;
	AbstractOutputStream main = null;
	for (; cnt < streams.size(); ++cnt)
	    {
	    main = StreamFactory.instance().createOutStream(streams.get(cnt).split("\\(|\\)"));
	    if (main != null)
		{ break; }
	    }

	if (main == null) { return null; }

	AbstractOutputStream head = main;
	for (++cnt; cnt < streams.size(); ++cnt)
	    {
	    final AbstractOutputStream chained = StreamFactory.instance().createOutStream(streams.get(cnt).split("\\(|\\)"));
	    if (chained != null)
		{
		head.chained(chained);
		head = chained;
		}
	    }

	return main;
	}

    public AbstractOutputStream get_stream(Severity.level severity)
	{
	return m_out_streams[severity.ordinal()];
	}
}
