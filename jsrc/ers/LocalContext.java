/** @file  LocalContext.java This file defines the ers LocalContext class,
  * which extends the ers Context interface.
 * @author Claire Biau
 *	This file defines the LocalContext class,
  * which implements the Context interface.
 */

package ers;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Date;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ManagementFactory;

import com.sun.security.auth.module.UnixSystem;

class LocalContext extends AbstractContext  {
	
	static int PID = 0 ; // PID of the current process, initialize only once

	/** Constructor - defines a local Issue context.
	  * This constructor should generally not be called directly, instead use the function ers.Issue.buildContext()
	  * @param filename name of the source code file
	  * @param line_number line_number in the source code
	  * @param function_name name of the function - either pretty printed or not
	  */
	public LocalContext ( final String package_name, final String filename, final int line_number, final String function_name )
	{
		final Throwable throwable = new Throwable();
		throwable.fillInStackTrace();
		m_stack = throwable.getStackTrace();
		m_stack_size = m_stack.length;
		
		m_package_name = package_name;
		m_application_name = get_application_name() ;
		m_file_name = filename;
		m_function_name = function_name;
		m_line_number = line_number;
		m_time = new Date().getTime();
		m_process_context = new ProcessContext(get_host_name(), getpid(), Thread.currentThread().getId(), get_cwd(), getuid(), get_user_name());
	}
	
	/** Constructor - defines a local Issue context.
	  * 
	  * @param context construction by copy
	  */
	public LocalContext ( final LocalContext context )
	{
		m_package_name = context.package_name();
		m_application_name = context.application_name();
		m_file_name = context.file_name();
		m_function_name = context.function_name();
		m_line_number = context.line_number();

		m_process_context = new ProcessContext(context.m_process_context) ;
	}

	@Override
	public String toString() {
		return "LocalContext [\n m_process_context=" + m_process_context
				+ ",\n m_package_name=" + m_package_name + ",\n m_application_name=" + m_application_name
				+ ",\n m_file_name=" + m_file_name + ",\n m_function_name=" + m_function_name
				+ ",\n m_line_number=" + m_line_number + ",\n m_stack=" + Arrays.toString(m_stack)
				+ ",\n m_stack_size=" + m_stack_size + ",\n m_time=" + m_time + ",\n cwd()=" + cwd()
				+ ",\n host_name()=" + host_name() + ",\n process_id()=" + process_id()
				+ ",\n thread_id()=" + thread_id() + ",\n user_id()=" + user_id()
				+ ",\n user_name()=" + user_name()
				+ "\n]";
	}

	public String cwd () { return m_process_context.cwd; }		/**@return current working directory of the process */
	public String file_name () { return m_file_name; }			/**@return name of the file which created the issue */
	public String function_name () { return m_function_name; }		/**@return name of the function which created the issue */
	public String host_name() { return m_process_context.hostname; }	/**@return host where the process is running */
	public int line_number () { return m_line_number; }			/**@return line number, in which the issue has been created */
	public String package_name() { return m_package_name; }			/**@return Java package name */
	public int process_id () { return m_process_context.pid; }		/**@return process id */
	public long thread_id () { return m_process_context.tid; }		/**@return thread id */
	public StackTraceElement[] stack_symbols() { return m_stack;}		/**@return stack frames */
	public int stack_size () { return m_stack_size;}			/**@return number of frames in stack */
 	public long user_id () { return m_process_context.uid; }		/**@return user id */
	public String user_name () { return m_process_context.uname; }	/**@return user name */
	public long time () { return m_time; }					/**@return Unix time of the issue */
        public String application_name() { return get_application_name(); }	/**@return Application name */

	public static String get_cwd()
	{		
		return java.lang.System.getProperty("user.dir");
	}
	
	public static String get_user_name()
	{
		return java.lang.System.getProperty("user.name");
	}

	public static String get_application_name()
	{
		String appliName = java.lang.System.getenv("TDAQ_APPLICATION_NAME");
		if (appliName == null) {
			appliName = "unknown";
		}
		return appliName ;
	}
	
	public static String get_host_name() 
	{
		try {
			final InetAddress address = InetAddress.getLocalHost();
			return address.getHostName();
		} catch (UnknownHostException e) {
			return "unknown" ;
		}		
	}
	
	public static long getuid()
	{
		return new UnixSystem().getUid();		
	}
	
	// TODO when moving to newer Java, use ProcessHandle API
	public static synchronized int getpid()
	{	
		if ( PID != 0 ) return PID ;

		final RuntimeMXBean rmxb = ManagementFactory.getRuntimeMXBean();
		final String token = rmxb.getName() ; // returns pid@hostname
		int idx = token.indexOf('@') ;
		if ( idx != -1 ) {
			String pid = token.substring(0, idx - 1) ;
			try {
				PID = Integer.parseInt(pid) ;
			}
			catch ( NumberFormatException ex ) {
				java.lang.System.err.println("Failed to get PID from string " + token);
			} 

		} else {
			java.lang.System.err.println("@ not found in RuntimeMXBean " + token) ;
		}
		if ( PID == 0 ) { // fallback to something meaningful 
			try {
				PID = Math.toIntExact(Thread.currentThread().getId()) ;
			} catch ( final ArithmeticException ex ) {
				PID = 1 ;
			}
			
		}
		return PID;
	}
	
	// constructs a context object with all the current values 
	public  ProcessContext 			m_process_context ;

	private final String			m_package_name;			/** source package name */
	private final String			m_application_name;		/** source application name */
	private final String			m_file_name;			/** source file-name */
	private final String			m_function_name;		/** source function name */
	private final int			m_line_number;			/** source line-number */

	private StackTraceElement[]		m_stack;			/** stack frames */
	private int				m_stack_size;			/** stack frames number */
	private long				m_time;				/** time of creating of context, UTC unix time from 1970 */
	
}
