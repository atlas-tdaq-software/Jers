package ers ;

public class BadStreamConfiguration extends Exception
{ 
  public BadStreamConfiguration()
  {
  super() ;
  }
		 
  public BadStreamConfiguration(final String err)
  {
    super("Bad configuration of a stream: " + err);
  }

  public BadStreamConfiguration(final String err, final Throwable cause)
  {
	super(err, cause) ;
  }
}
  
