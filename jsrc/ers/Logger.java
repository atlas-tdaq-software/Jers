/**
 * Package ers provides functionality of the Error Reporting System (http://atlas-tdaq-monitoring.web.cern.ch/atlas-tdaq-monitoring/ERS/)
 * in Java language.
 * 
 * It's main public classes:
 * 
 * On the reporter side:
 * ers.Issue: use it to create your Issues or declare a specific subclass of ers.Issue with named parameters
 * (somehow equivalent to use of ERS_DECRARE_ISSUE macro)
 * ers.System: use it to send Issues to ERS streams
 * @see examples/TestReporter.java for example of reporting the issues.
 * 
 * On the receiver side:
 * ers.Receiver: use it to implement your receiver callback
 * ers.StreamManager.instance().add_receiver to add your receiver to an input stream (e.g. 'mrs')
 * @see examples/TestReceiver.java for example of subscribing to 'mrs' input stream and receiving the issues.
 * 
 * On the side of a developer providing an ERS stream:
 * ers.StreamRegistrator
 * ers.OutputStream
 * ers.InputStream
 */
package ers;

/** @file Logger.java This file defines the ers.Logger class, 
 * @author Claire Biau, Andrei Kazarov
 */

/**
 * ers.Logger is the main entry point to ERS in java and it is used to report messages to ERS.
 * Messages are reported as instances of ers.Issue or derived class,
 * with exception of LOG and DEBUG messages which can be reported as Strings.
 * See example of use in examples/TestReporter.java
 */
public final class Logger {

	private Logger() {} ;

	/** Sends an Exception to the FATAL stream 
	 * @param issue 
	 */
	public static void fatal( final Exception exception )
	{
		StreamManager.instance().fatal(exception);
	}
	
	/** Sends an Issue to the ERROR stream 
	 * @param issue 
	 */
	public static void error( final Exception issue )
	{
		StreamManager.instance().error(issue);
	}
	
	/** Sends an Issue to the WARNING stream 
	 * @param issue 
	 */
	public static void warning( final Exception issue )
	{
		StreamManager.instance().warning(issue);
	}
	
	/** Sends an Issue to the DEBUG stream 
	 * @param issue 
	 * @param level
	 */
	public static void debug( final int level, final Exception issue )
	{ 
		if (Configuration.instance().debugLevel() >= level)
			{
			StreamManager.instance().debug(issue, level);
	    		}
	}
	
	/** Sends an Issue as a string to the DEBUG stream 
	 * @param string 
	 * @param level
	 */
	public static void debug( final int level, final String string )
	{ 
		if (Configuration.instance().debugLevel() >= level)
			{
			StreamManager.instance().debug(new Issue(string), level) ;
			}
	}
	
	/** Sends an Issue to the INFO stream 
	 * @param issue 
	 */
	public static void info( final Exception issue )
	{
		StreamManager.instance().information(issue);
	}
	
	/** Sends a text message as simple Issue as a string to the INFO stream 
	 * @param message 
	 */
	public static void info( final String message )
	{ 
		StreamManager.instance().information(new Issue(message));
	}

	/** Sends an Issue to the LOG stream 
	 * @param issue 
	 */
	public static void log( final Exception issue )
	{
		StreamManager.instance().log(issue);
	}
	
	/** Sends an text message to the LOG stream 
	 * @param message your message 
	 */
	public static void log( final String message )
	{
		StreamManager.instance().log(new Issue(message));
	}
	
	/** @return the configured verbosity level
	 */
	public static int verbosityLevel( )
	{
		return Configuration.instance().verbosityLevel( );
	}
}
