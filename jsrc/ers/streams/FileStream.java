/*
 *  StandardStreamOutput.java
 *  
 *  Created by Claire Biau on 02.06.08
 *  Copyright 2008-2024 CERN for the benefit of the ATLAS Collaboration.
 *  All rights reserved.
 */



package ers.streams;

import java.io.FileOutputStream;
import java.util.Map;

import ers.ErsStreamName;

import ers.Logger;

/** This class defines standard stream output function for the ERS streams. It extends from OutputStream.
 *	This defines print function for output an issue to any standard .output stream.
 */
@ErsStreamName(name="file")
public class FileStream extends ers.AbstractOutputStream {
	
	@Override
	public void configure(final Map<String, String> config) {
		
	}

	private java.io.FileOutputStream stream;
	private String filename;

	/**
	 * constructor
	 * @param filename
	 * @param append
	 */
	public FileStream( final String filename, final String append ) throws java.io.FileNotFoundException
		{
		super() ;
		this.filename = filename ;
		stream = new FileOutputStream(filename, Boolean.parseBoolean(append)) ;		
		}

	/**
	 * constructor
	 * @param filename
	 */
	public FileStream( final String filename ) throws java.io.FileNotFoundException
		{
		this(filename, "true") ;	
		}

	/**
	 * This function writes the issue given in attribute on the standartStreamOutput
	 * It writes is in a human readable way
	 * @param issue
	 */
	synchronized public void write(final Exception issue, final ers.Severity severity) {
		try 	{
			ers.IssuePrinter.println(stream, issue, severity, Logger.verbosityLevel()) ;
			chained().write( issue, severity );	
			}
		catch ( java.io.IOException ex )
			{
			System.err.println("Failed to write issue to file stream: " + ex) ;
			}
	}
	
	public void cleanUp()	{
		try 	{
			stream.flush() ;
			stream.close() ;
			}
		catch ( java.io.IOException ex )
			{
			System.err.println("Failed to close file " + filename + ": " + ex) ;
			}
	} 
}
