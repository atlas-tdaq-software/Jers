/**
 * @file NullStream.java
 * @author Claire Biau
 * @date 2008
 * @copyright 2008-2024 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 */

package ers.streams ;

import java.util.Map ;

import ers.AbstractOutputStream ;
import ers.ErsStreamName ;

/** This stream class implements a null stream, i.e a stream where no issue can be read from
 * and it also silently discards sent issues.
 */
@ErsStreamName(name="null")
public class NullStream extends AbstractOutputStream {

	/**
	 * Does essentially nothing
	 */
	public void write (final Exception issue, final ers.Severity sev) {}
	
	/**
	 * This function returns true because a NullStream is always considered as null
	 */
	public boolean isNull() {
		return true;
	}

	public void cleanUp() {} ;

	public void configure(Map<String, String> config) {
	}
}
