/**
 * @file StandardFilterStream.java
 * @author Andrei Kazarov
 * @date 2008
 * @copyright 2008-2024 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 */

package ers.streams ;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ers.Issue;
import ers.ErsStreamName ;

/** Defines standard filter output function for the ERS streams. It extends from OutputStream.
 * Examples of filtering expression: "!TEST_QUAL,!TestStreams" e.g. simple logical chain using package name and qualifiers
 * Elements with ! define a black list, without ! - white list, so
 * Issue is accepted by the filter if its qualifiers (either) are not in the black list, and if at least one is in the
 * white list (or the white list is empty).
 */
@ErsStreamName(name="filter")
public class StandardFilterStream extends ers.AbstractOutputStream {
	
	@Override
	public void configure(final Map<String, String> config) {
		// TODO Auto-generated method stub	
	}

	private List<String> m_include ;		
	private List<String> m_exclude ;		


	public void cleanUp()	{}
	/**
	 * constructor
	 * @param format filtering format, e.g. "!TEST_QUAL,!BadQualifier,GoodQualifier"
	 */
	public StandardFilterStream( String format )
	{	
		m_exclude = new ArrayList<String>() ;
		m_include = new ArrayList<String>() ;

		String[] tokens = format.split(",") ;
    		for( String t: tokens )
    			{
    			if ( !t.isEmpty() && t.charAt(0) == '!' )
           			m_exclude.add( t.substring(1) );
        		else
            		m_include.add( t );
    			}			
	}
	
	/**
	 * This function writes the issue given in attribute on the standartStreamOutput
	 * It writes is in a human readable way
	 * @param issue
	 */
	synchronized public void write( final Exception exception, final ers.Severity sev ) {
		if ( exception instanceof Issue )
		    {
		    final Issue issue = (Issue)exception ; 
		    if ( isAccepted(issue) ) { chained().write( issue, sev ) ; }
		    }
	}

	private boolean isAccepted( final ers.Issue issue ) {
		for ( String ex: m_exclude ) { // first black list
			for ( String q: issue.qualifiers( ) )
				{ if ( q.equals(ex) ) return false ; }
		} 
    
   		for ( String in: m_include ) { // then white list
			for ( String q: issue.qualifiers( ) )
   				{ if ( q.equals(in) ) return true ; }
		}

	    return m_include.isEmpty() ; // if here, true only if white list is empty
    	}
}
