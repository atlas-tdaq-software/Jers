/**
 * @file ThrottleStream.java
 * @author Andrei Kazarov
 * @date 2008
 * @copyright 2008-2024 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 */

package ers.streams ;

import java.util.Map;

import ers.ErsStreamName;

/**
 * This class defines ERS throttle stream. It extends ers.OutputStream. Present dummy implementation works in pass-through mode.
 */
@ErsStreamName(name="throttle")
public class ThrottleStream extends ers.AbstractOutputStream {
	
	public void cleanUp() {} ;

	/**
	 * configure the stream
	 * @param config
	 */
	public void configure ( Map<String,String> config) {} ;

	public ThrottleStream( String format ) {} ;

	public void write(final Exception issue, final ers.Severity sev) {
		chained().write( issue, sev ) ;	 
	}

}
