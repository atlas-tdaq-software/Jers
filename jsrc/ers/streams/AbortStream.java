/*
 *  AbortStream.java
 *  
 *  Created by Andrei Kazarov
 *  Copyright 2011-2024 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 *
 */

/** \file  AbortStream.java This file defines standard ERS abort stream. It extends from OutputStream.
 * \author Andrei Kazarov
 *	
 */

package ers.streams ;

import java.util.Map;

import ers.ErsStreamName;

@ErsStreamName(name="abort")
public class AbortStream extends ers.AbstractOutputStream {
	
	/**
	 * clean the stream, called from shutdown hook thread
	 */
	public void cleanUp() {} ;

	/**
	 * configure the stream
	 * @param config
	 */
	public void configure ( final Map<String,String> config) {} ;

	public void write(final Exception issue, ers.Severity sev) {
			 new Throwable("Issue '" + issue.toString() + "' received by abort stream. Aborting the program.").printStackTrace();
			 System.exit(1);
	}

}
