/** @file  StandardStreamError.java This file defines standard locked error stream (lstderr). It extends from OutputStream.
 * @author Andrei Kazarov
 * @date 2008
 * @copyright 2008-2024 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 */

package ers.streams ;

import java.util.Map;

import ers.ErsStreamName;

/**
 * Defines ERS output stream for 'lstderr' stream name. Implemented as IssuePrinter for java.lang.System.err.
 * @see ers.IssuePrinter
 */
@ErsStreamName(name="lstderr")
public class StandardStreamError extends ers.AbstractOutputStream {
	
	@Override
	public void configure(Map<String, String> config) {
		// TODO Auto-generated method stub	
	}

	public void cleanUp()	{}

	private java.io.PrintStream stream;

	/**
	 * Default constructor
	 */
	public StandardStreamError( )
	{	
		stream = java.lang.System.err;
	}
	
	/**
	 * This function writes the given issue on the standartStreamOutput using IssuePrinter
	 * It writes is in a human readable way
	 * @param issue
	 */
	synchronized public void write( final Exception issue, final ers.Severity sev ) {
		try {
			ers.IssuePrinter.println(stream, issue, sev, ers.Logger.verbosityLevel()) ;	
			chained().write( issue, sev ); // pass to the next stream in chain	
		} catch ( final java.io.IOException ex ) {
			java.lang.System.err.println("Failed to write issue to a print stream: " + ex) ;
		}
	}
}
