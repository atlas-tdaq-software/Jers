/** @file  StandardStreamOutput.java This file defines standard ERS locked output stream (lstdout). It extends from OutputStream.
 * @author Andrei Kazarov
 * @date 2008
 * @copyright 2008-2024 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 */

package ers.streams ;

import java.util.Map;

import ers.ErsStreamName;

/**
 * Defines ERS output stream for 'lstdout' stream name. Implemented as IssuePrinter for java.lang.System.out.
 * @see ers.IssuePrinter
 */
@ErsStreamName(name="lstdout")
public class StandardStreamOutput extends ers.AbstractOutputStream {
	
	@Override
	public void configure(final Map<String, String> config) {
	}

	private final java.io.PrintStream stream ;

	public void cleanUp()	{}

	/**
	 * Default constructor
	 */
	public StandardStreamOutput( )
	{	
		super() ;
		stream = java.lang.System.out ;
	}
	
	/**
	 * This function writes the issue given in attribute on the standartStreamOutput
	 * It writes is in a human readable way
	 * @param issue 
	 * @param sev Severity 
	 */
	synchronized public void write(final Exception issue, final ers.Severity sev) {
		try {
			ers.IssuePrinter.println(stream, issue, sev, ers.Logger.verbosityLevel()) ;
			chained().write( issue, sev ); // next stream	
		} catch ( final java.io.IOException ex ) {
			System.err.println("Failed to write issue to std output stream: " + ex) ;
		}
	}
}
