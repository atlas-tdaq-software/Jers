package ers;

/**
 *  @file IssueReceiver.java
 *
 *  @author Claire Biau
 *  @date 02.06.08
 *  @copyright 2008 CERN. All rights reserved.
 */

/**
 *  Declares ERS interface for receiving the messages. User needs to define and instantiate his own Receiver.
 *  See how to use the class in examples/TestReceiver.java
 */
public interface IssueReceiver {

	/** @brief called back when the new issue is received by the framework*/
	void receive( Issue issue );
}
