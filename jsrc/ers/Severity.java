/*
 *  Severity.java
 *  
 *  Created by Claire Biau on 02.06.08
 *  Copyright 2008 CERN. All rights reserved.
 *
 */

/** @file  Severity.java This file defines Severity type for ERS.
 * @author Andrei Kazarov
 */

package ers;

/**
defines Severity type for ERS, corresponding to "DEBUG", "LOG", "INFORMATION", "WARNING", "ERROR", "FATAL"
ERS reporting streams.
*/
public class Severity {
	
	public enum level { Debug, Log, Information, Warning, Error, Fatal ; } ;

	//public static String severityNames[] = { "DEBUG", "LOG", "INFORMATION", "WARNING", "ERROR", "FATAL" };
	
	private final int 	m_rank; 	// used for Debug only
	private final level 	m_level; 	// 0 to 5, we need this to be able to de-serialize issues from e.g. MRS where they passed as ints - constructor 
	 
	 /**
	  * Constructor
	  * @param severity
	  * @param level: used to de-serialize Severity from int
	  * @param rank: used only for severity DEBUG, i.e. DEBUG_0 DEBUG_1 etc 
	  */
	 Severity( final level _level, final int _rank )
	 {
		m_level = _level ;
		m_rank = _rank ;
	 }

	 /**
	  * Constructor
	  * @param severity
	  * @param level: used to de-serialize Severity from int
	  */
	 public Severity( final level _level )
	 {
		m_level = _level ;
		m_rank = 0 ;
	 }
	 
	 /** String representation of Severity, used for output.
	  * Capitalized enumeration names with exception of Information with is returned as INFO
	  * @return severity level
	  */
	 public String toString( )
	 {
		if ( m_level.equals(level.Debug) )
			{
			final StringBuffer out = new StringBuffer(m_level.name().toUpperCase()) ;
			out.append('_').append(m_rank);
			return out.toString();
			}
		if ( m_level.equals(level.Information) )
			{
			return "INFO" ;
			}
		return m_level.name().toUpperCase();
	 }

	 /**
	  * @return severity level as integer
	  */
	 public int toInt( )
	 {
		return m_level.ordinal() ;
	 }
	 
	 /**
	  * @return severity level as enumeration
	  */
	 public Severity.level level()
	 {
		 return m_level;
	 }	
	 
	 /**
	  * @return int debug rank of severity
	  */
	 public int getRank() {
		return m_rank;
	 }  	 

}
