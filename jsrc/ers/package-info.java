/**
 * Package ers provides functionality of the Error Reporting System (<a href="http://atlas-tdaq-monitoring.web.cern.ch/atlas-tdaq-monitoring/ERS/">http://atlas-tdaq-monitoring.web.cern.ch/atlas-tdaq-monitoring/ERS/</a>)
 * in Java language.

 * <p>
 * It's main public classes:<br>
 * <br>
 * On the <i>reporter</i> side:<br>
 * <br>
 * {@link ers.Issue}: use it to create your Issues or declare a specific subclass of <verbatim>ers.Issue/<verbatim> with named parameters
 * (somehow equivalent to use of ERS_DECRARE_ISSUE macro) like in the following example:<br>
 * <br>
 * <code> 
 public class MyTestIssue extends ers.Issue <br>
	{<br>
	public String reason ;<br>
	public int port_number ;<br>
	<br>
	public MyTestIssue (String reason, int port_number, Issue cause)<br>
		{<br>
		super("MyTestIssue happened because of %s on a port number %d", reason, port_number, cause) ;<br>
		}<br>
<br>
	public MyTestIssue (String reason, int port_number)<br>
		{<br>
		super("MyTestIssue happened because of %s on a port number %d", reason, port_number) ;<br>
		}<br>
	}; <br>
 * </code>
 * <p>
 * {@link ers.Logger}: use it to send Issues to ERS streams:<br><br>
 * <code>
   ers.Logger.error(new MyTestIssue(param1, param2, new Issue("A funny cause") ) ) ;<br>
   ers.Logger.debug(0,"In-place string issue") ;<br>
   ers.Logger.log("In-place Log message") ;<br>
 </code>
 * <p>
 * See examples/TestReporter.java for example of reporting the different types of issues.
 * <p>
 * On the <i>receiver</i> side:<br><br>
 * {@link ers.IssueReceiver}: use it to implement your receiver callback
 * <p>
 * {@link ers.StreamManager}: call  {@link #instance().add_receiver} to add your receiver to an input stream (typically to 'mts').<br>
 * <p>
 * See examples/TestReceiver.java for example of subscribing to 'mts' input stream and receiving the issues:
 * <code>
 * 		StreamManager.instance().add_receiver(receiver, "mts", "*")<br>
		StreamManager.instance().add_receiver(receiver, "mts", "sev=ERROR or qual=Test")<br> 
		StreamManager.instance().add_receiver(receiver, "mts", "initial", "*")<br>
   </code>
   <p>
   See {@link mts}: a java implementation of MTS CORBA-based transport layer for distributing ERS messages in TDAQ system. 
 * <p>
 * On the side of a <i>developer providing an ERS stream</i>:<br><br>
 * {@link ers.StreamBase}: any ERS stream (either Input or Output) in Java must implement this interface in order to be loaded by the framework when program starts.
 * Jar file with the implementation must be in java classpath, of course.<br>
 * {@link ers.AbstractOutputStream}: interface for output streams.<br>
 * {@link ers.AbstractInputStream}: interface for output streams.<br>
 * <p>
 * The package also contains:<br>
 * <li>a number of standard ERS streams implementations: abort ({@link ers.streams.AbortStream}), file {@link ers.streams.FileStream}, null {@link ers.streams.NullStream},
 * filter {@link ers.streams.StandardFilterStream}, stdout {@link ers.streams.StandardStreamOutput}, stderr {@link ers.streams.StandardStreamError}, throttle {@link ers.streams.ThrottleStream}<br>
 * See {@link ers.streams} package.
 * <li>j4log appender, allowing reporting to ERS from j4log logger by third-party s/w. See examples/TestReporter.java.
 * 
 * @see mts 
 */
package ers;
