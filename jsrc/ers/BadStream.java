package ers ;

public class BadStream extends Exception
{
  private static final long serialVersionUID = 2L;
  
  public BadStream()
  {
  super() ;
  }
 
  public BadStream(final String err)
  {
  super(err);     // call super class constructor
  }

  public BadStream(final String err, final Throwable cause)
  {
	super(err, cause) ;     // call super class constructor
  }
}