/*
 *  Configuration.java
 *  
 *  Created by Claire Biau on 02.06.08
 *  Copyright 2008 CERN. All rights reserved.
 *
 */

/** \file  Configuration.java This file defines ERS Configuration class.
 * \author Claire Biau
 *	The Configuration class provides API for configuring ERS output streams.
 */

package ers;

import java.util.HashMap;
import java.util.Map;

public final class Configuration {	
	private static Configuration instance = null;
	private transient int debugLevel ;              /** @brief current active level for the debug stream */	
	private transient int verbosityLevel ; 		/** @brief current verbosity level for all streams */
	
	/** Private constructor - can not be called by user code, use the \c instance() method instead
	  * @see instance() 
	  */
	private Configuration( )
	{
		debugLevel = readEnvironment( "TDAQ_ERS_DEBUG_LEVEL", 0 );
		verbosityLevel  = readEnvironment( "TDAQ_ERS_VERBOSITY_LEVEL", 0 );
	}
	
	/** @brief return the instance of configuration */
	synchronized public static Configuration instance()
	{
		if( instance == null ){
			instance = new Configuration();
		}
		return instance;
	}
	
	/**@brief returns current debug level */
	public int debugLevel()                 
	{ return debugLevel; }
		     
	public int debug_level()                 
	{ return debugLevel; }

	/**@brief sets verbosity level */
	public void debugLevel( final int debug_level )     
	{ debugLevel = debug_level; }
	        
	public void debug_level( final int debug_level )     
	{ debugLevel = debug_level; }

	/**@brief can be used to set the current verbosity level */
	public int verbosityLevel()              
	{ return verbosityLevel ; }

	public int verbosity_level()              
	{ return verbosityLevel ; }
	
	/**@brief can be used to set the current verbosity level, e.g. via IPC command */
	public void verbosityLevel( final int verbosity_level )
	{
		verbosityLevel  = verbosity_level;
		final Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("verbosity", String.valueOf(verbosityLevel ));
		StreamManager.instance().configure_all_output_stream( parameters );
	}

	public void verbosity_level( final int verbosity_level )
	{
	verbosityLevel(verbosity_level) ;
	}
	
	/**
	 * This function reads the environment variables 
	 */
	public int readEnvironment( final String name, final int defval )
	{
		final String env = java.lang.System.getenv( name );		
		if ( env != null ) {
		try	{
			return Integer.parseInt(env) ;
			}
		catch ( NumberFormatException ex )
			{
			ers.Logger.error(new Issue("Failed to parse " + name + " environment: " + env, ex) ) ;
			} }
		
		return defval ;
	}	
}



