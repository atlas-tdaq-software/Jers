/*
 *  LocalProcessContext.java
 *  
 *  Created by Claire Biau on 02.06.08
 *  Copyright 2008 CERN. All rights reserved.
 *
 */

/** \file  ProcessContext.java This file defines process context
 * \author Claire Biau
 *	
 */

package ers;

public class ProcessContext
{

    public transient String hostname;
    /** host name */
    public transient int pid;
    /** process id */
    public transient long tid;
    /** thread id */
    public transient String cwd;
    /** process cwd */
    public transient long uid;
    /** user id */
    public transient String uname;

    /** user name */

    public ProcessContext(final String _hostname, final int _pid,
	    final long _tid, final String _cwd, final long _uid, final String _uname)
	{
	hostname = _hostname;
	pid = _pid;
	tid = _tid;
	cwd = _cwd;
	uid = _uid;
	uname = _uname;
	}

    public ProcessContext(final ProcessContext pcont)
	{
	hostname = pcont.hostname;
	pid = pcont.pid;
	tid = pcont.tid;
	cwd = pcont.cwd;
	uid = pcont.uid;
	uname = pcont.uname;
	}
}
