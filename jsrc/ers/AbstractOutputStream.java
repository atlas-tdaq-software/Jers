/** @file  OutputStream.java
 * @author Claire Biau, Andrei Kazarov
 * @version 2.0
 */

package ers;

import java.util.Map;
import ers.streams.NullStream ;

/**
 * ERS output stream abstract interface.
 * An ERS output stream is a mean to send ers Issues or java Exceptions.
 * This interface defines two core methods to write issues and to configure the stream.
 * Certain subclasses of stream might implement only sending while leaving the configure method empty.
 * Implementation classes must also realize @ProviderFor(StreamRegistrator.class) interface (getStreamName method) in order to be used by the framework.
 * see realizations of different streams in Jers/src/ers/streams/*.java and in mts/jsrc/mts/MTSOutStream.java
 */
public abstract class AbstractOutputStream implements StreamBase {

	private transient AbstractOutputStream chainedStream ;
	
	public AbstractOutputStream () { super() ; } 
	
	/**
	 * Sends an Exception into the stream, may be Issue or base java Exception
	 * In latter case, use esr.Issue.get_context(issue) to get the context for sending it out 
	 * @param issue Exception
	 * @param severity Severity of the issue (derived from the stream it was sent to)
	 */
	public abstract void write ( Exception issue, Severity severity );
	
	/**
	 * configure the stream
	 * @param confug a configuration of a stream 
	 */
	public abstract void configure ( Map<String,String> config) /*throw ()  TODO*/;
	
	/**
	 * Gets the chained stream if it exists
	 * @return the chained stream or NullStream
	 */
	protected AbstractOutputStream chained ()
		{
		if ( chainedStream == null ) chainedStream = new NullStream() ;
		return chainedStream ;
		}
	
	/** Adds a chained stream 
	 * @param stream to be chained */
	public void chained ( final AbstractOutputStream stream )
	{
		chainedStream = stream;
	}

	/**
	 * This function returns false because an OutputStream is not considered as null
	 * @return false
	 */
	protected boolean isNull ()
	{
		return false;
	}

	public void configureStreamChain ( final Map<String,String> config )
	{
		configure ( config );
		if (!chained().isNull())
			{ chained().configureStreamChain(config); }
	}
}
