package ers ;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for classes that are registered as ERS streams at application startup, @see StreamManager
 * and then created in according to configuration @see StreamFactory
 * 
 * @param name name of the stream, used later by ERS framework to instantiate streams in runtime according to configuration
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ErsStreamName {
    String name() ;
}