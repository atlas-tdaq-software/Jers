/*
 *  LocalContext.java
 *  
 *  Created by Claire Biau on 02.06.08
 *  Copyright 2008 CERN. All rights reserved.
 *
 */

/** \file  LocalContext.java This file defines the ers LocalContext class,
  * which extends the ers Context interface.
 * \author Claire Biau
 *	This file defines the LocalContext class,
  * which implements the Context interface.
 */

package ers;

public class RemoteContext extends AbstractContext {
	
	/** Constructor - defines a remote Issue context.
	  * @param filename name of the source code file
	  * @param line_number line_number in the source code
	  * @param function_name name of the function - either pretty printed or not
	  */
	public RemoteContext ( 	final String package_name, final String file_name, final int line_number, final String function_name,
				final String app_name, final ProcessContext p_context, final long _time )
	{
		stackSize = 0;
		stack = new StackTraceElement[stackSize];
		
		packageName = package_name ;
		applicationName = app_name ;
		fileName = file_name ;
		functionName = function_name ;
		lineNumber = line_number ;
		time = _time ;

		processContext = new ProcessContext(p_context) ;
	}
	
//	public Context clone () { return new RemoteContext ( this ); }		/**< \return copy of the current context */

    /** @return name of the file which created the issue */
    public String file_name()
	{
	return fileName;
	}

    /** @return name of the function which created the issue */
    public String function_name()
	{
	return functionName;
	}

    /** @return line number, in which the issue has been created */
    public int line_number()
	{
	return lineNumber;
	}

    /** @return CMT package name */
    public String package_name()
	{
	return packageName;
	}

    /** @return process call stack */
    public StackTraceElement[] stack_symbols()
	{
	return stack;
	}

    /** @return number of stack frames */
    public int stack_size()
	{
	return stackSize;
	}

    /** @return Unix time of the issue */
    public long time()
	{
	return time;
	}

    /** @return application name */
    public String application_name()
	{
	return applicationName;
	}

    /**
     * 
     * @return Context of the process
     */
    public ProcessContext process_context() { return processContext; }
	
	public String cwd()
	{		
		return processContext.cwd ;
	}

	public String user_name()
	{
		return processContext.uname ;
	}
	
	public String host_name() 
	{
		return processContext.hostname ;
	}
	
	public long user_id()
	{
		return processContext.uid;		
	}
	
	public int process_id()
	{	
		return processContext.pid;
	}

	public long thread_id()
	{	
		return processContext.tid;
	}

	// remote context
	private final String					packageName ;			/** source package name */
	private final String					applicationName ;		/** source application name */
	private final String					fileName ;			/** source file-name */
	private final String					functionName ;			/** source function name */
	private final int					lineNumber;			/** source line-number */
	private final StackTraceElement[]			stack;				/** stack frames */
	private final int					stackSize;			/** stack frames number */
	private final long					time;				/** time of creating of context, UTC unix time from 1970 */

	// remote process context
	private final ProcessContext 				processContext ;		/** process context */
}
