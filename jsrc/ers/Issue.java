/**
 *	@file  Issue.java
 *  
 *  @author Claire Biau on 02.06.08
 *  @author Andrei Kazarov 2013
 *  @author Lykourgos Papaevgeniou 08.2013
 *
 *  Copyright 2008-2024 CERN. All rights reserved.
 *
 */

package ers;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.List;
import java.util.ArrayList;
import java.lang.reflect.*;
import java.util.IllegalFormatException;

import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.ThrowableInformation;

/**
 * ers.Issue is the base class for any user defined issue. See example of use in
 * examples/TestReporter.java and examples/MyTestIssue.java
 */
public class Issue extends Exception {
	private static final long serialVersionUID = 1L;

	// "common" private init method with Context, used by other constructors
	private void init(final AbstractContext context) {
            m_class = this.getClass();
            m_id = this.getClass().getName().replaceAll("\\.", "::");
    
            // default severity, it will be changed at output according to ers
            // output stream
            m_severity = new Severity(Severity.level.Debug, 0);
    
            m_values = new HashMap<String, String>();
            m_qualifiers = new ArrayList<String>();
            m_params_names = new ArrayList<String>();
    
            m_time = new Date().getTime();
            // Returns the number of milliseconds since January 1, 1970, 00:00:00 GMT
            m_cause = null;
            m_context = context;
    
            addDefaultQualifiers();
	}
	
	// "common" private init method, used by other constructors
	private void init() {
            init(buildContext(this));
    
            // build m_parameters_names which is used in Object... constructor
            for(Field f : m_class.getDeclaredFields()) {
                if(m_class.equals(ers.Issue.class)) {
                    continue;
                } // ignore base class members
                final String pname = f.getName();
                m_params_names.add(pname);
            }
	}

	private void init(final String message) {
	    init();
	    m_message = message;
	}

        private void init(final String message, final Throwable cause) {
            init(message);
        
            if(cause instanceof Issue) {
                setCause((Issue) cause);
            } else {
                setCause(new Issue(cause));		
            }
        }	
	
	/**
	 * Constructor - Given a LoggingEvent (log4j), creates an issue
	 * 
	 * @param log4jEvent
	 *             the event created by log4j API
	 */	
        public Issue(final LoggingEvent log4jEvent) {
            super(log4jEvent.getRenderedMessage(),
                  (log4jEvent.getThrowableInformation() != null) ? log4jEvent.getThrowableInformation().getThrowable() : null);
    
            // TODO parseInt exceptions?
            init(new LocalContext(log4jEvent.getLocationInformation().getClassName(),
                                  log4jEvent.getLocationInformation().getFileName(),
                                  Integer.parseInt(log4jEvent.getLocationInformation().getLineNumber()),
                                  log4jEvent.getLocationInformation().getMethodName()));
    
            m_time = log4jEvent.timeStamp;
    
            final ThrowableInformation ti = log4jEvent.getThrowableInformation();
            if(ti != null) {
                setCause(new Issue(ti.getThrowable()));
            }                
            
            // timeStamp: The number of milliseconds elapsed from 1/1/1970 until logging event was created.
            // should use getTimestamp(), but it only exists since log4j 1.2.15
    
            // build m_parameters_names which is used in Object... constructor
            // for (Field f : m_class.getDeclaredFields()) {
            // if (m_class.equals(ers.Issue.class))
            // continue; // ignore base class memebers
            // String pname = f.getName();
            // ers.Logger.debug(3, "Class " + m_class.getName()
            // + " declared field with name " + f.getName());
            // m_parameters_names.add(pname);
            // }
    
            // add_default_qualifiers();
        }

	/**
	 * Constructor from string message
	 * 
	 * @param message
	 *            the user message associated with this issue
	 */
	public Issue(final String message) {
	    super(message) ;
	    init(message);
	}

	/**
	 * Constructor from Exception Since Exception may have Throwable cause, we
	 * need to encapsulate it to be able to (de)serialize
	 * 
	 * @param exception original Java Exception
	 */
	public Issue(final Throwable exception) {
            super(exception);
            
	    // there is no separate constructor with Issue as parameter,
	    // so we create new default Issue and set the Issue exception as the cause 
	    // when called with ers.Issue, 
	    if ( exception instanceof Issue ) {
	    	init() ;
		setCause((Issue)exception) ;
		return ;
	    }
	    
	    // otherwise we construt new Issue from raw Throwable
            init(exception.getLocalizedMessage());
    
            // nice ID for standard Java exceptions
            // NB: this formatting is done for C++ ERS compatibility, but it breaks "Java" look & feel - but only
            // when getID() or toString() are used for printing the Issue
            m_id = exception.toString().split(":")[0].replaceAll("\\.", "::");
    
            setContext(buildContext(exception));
            
            final Throwable cause = exception.getCause();
            if(cause != null) {
                m_cause = new Issue(cause);
            }
	}
	
	/**
	 * Constructor - takes an exceptions as the cause for the current exception.
	 * 
	 * @param message
	 *            the user message associated with this issue as String
	 * @param cause
	 *            Exception that caused the current Issue
	 */
	public Issue(final String message, final Throwable cause) {
	    super(message, cause) ;
 	    init(message, cause) ;
	}
	
	/**
	 * Constructor with formatted message and variadic parameters This
	 * constructor is not to be used directly (though it is public): users need
	 * to inherit from Issue class and call super(format_message, params) in the
	 * first line of the constructor. See examples/MyTestIssue.java
	 * 
	 * @param format_message
	 *            printf-like formatted message, using parameters in the
	 *            specified order, See
	 *            http://docs.oracle.com/javase/7/docs/api/java
	 *            /util/Formatter.html#syntax
	 * @param params
	 *            any parameters of the issue, usually contributing to the
	 *            formatted message. Last parameter can be Issue or Exception.
	 */
        public Issue(final String format_message, final Object... params) {
            super(Issue.formatMessage(format_message, params),
                  params[params.length - 1] instanceof Throwable ? (Throwable) params[params.length - 1] : null);
 
            int params_size = params.length;
            boolean has_cause = false;
            Throwable cause = null;
            final String message = Issue.formatMessage(format_message, params);
     
            if(params[params.length - 1] instanceof Throwable) {
                params_size--;
                has_cause = true;
                cause = (Throwable) params[params.length - 1];
            }
    
            // call base constructor
            if(has_cause) {
                init(message, cause);
            } else {
                init(message);
            }
    
            assert (params_size == m_params_names.size());
    
            // fill in parameters
            int idx = 0;
            for(Object param : params) {
                if(!(param instanceof Exception)) // skip last argument
                {
                    ers.Logger.debug(3, "Actual parameter: " + m_params_names.get(idx) + " value: " + param);
                    setValue(m_params_names.get(idx++), param);
                }
            }
    
            /*
             * // recognize and fill issue parameters names (m_parameters_names from user Constructor) and values (from issue_params)
             * Constructor<?>[] constrs = this.getClass().getDeclaredConstructors() ; assert( constrs.length == 1 ) ; Class<?>[] cons_params =
             * constrs[0].getParameterTypes() ; i = 0 ; for ( Object aparam: issue_params ) { set_value(cons_params[i++].getName(), aparam) ; }
             * java.lang.System.out.println("Calling constructor of " + this.getClass().toString() + " with variadic number of args, found " +
             * constrs.length + " constructors") ; for ( Constructor<?> ac: constrs) { java.lang.System.out.println("Lookin in constructor: " +
             * ac.toString()) ; cons_params = ac.getParameterTypes() ; int indx = 0 ; for ( Class<?> ap: cons_params ) {
             * java.lang.System.out.println("Declared constructor issue parameter: " + ap.getName() + ", actual value: " + params[indx]) ;
             * indx++ ; } }
             */
        }

	/**
	 * Issue factory, to be used to reconstruct an issue on remote (receiver side)
	 * as instance of a user's class. The class needs to be loaded, i.e. be
	 * present in CLASSPATH. This class is to be used only by implementations of
	 * InputStream, not by general users.
	 * 
	 * @param context
	 *            the context of the Issue, e.g where in the code did the issue
	 *            appear
	 * @param message
	 *            the user message associated with this issue
	 * @param cause
	 *            exception that caused the current Issue
	 */
	static public Issue createIssue(final RemoteContext context, final String issue_id,
	                                final String message, final Issue cause, final Severity.level severity, final long time,
	                                final List<String> qualifiers, final Map<String, String> parameters) throws BadIssue 
	{
            Issue an_issue;
            // Class<? extends Issue> issue_class = Issue.class;
            boolean no_class = true;
            // Constructor<? extends Issue> constr ;
    
            // need to revert fom ID to FQ class name (m_id = this.getClass().getName().replaceAll("\\.", "::"))
            // String classname = issue_id.replace("::",".") ;
    
            // TODO select proper constructor from user's class, if possible. E.g. it can be an annotated constructor?
            //
            // try {
            // issue_class = Class.forName(classname).asSubclass(ers.Issue.class) ;
            // no_class = false ;
            //
            // System.out.println("Found user Issue class: " + issue_class) ;
            //
            // Constructor<?> constrs[] = issue_class.getConstructors() ;
            // if ( constrs.empty() ) ;
            // {
            // System.err.println("User Issue class " + issue_class + " has no public constructors. Will use base ers.Issue class.") ;
            // no_class = true ;
            // issue_class = Issue.class ;
            // }
            // else
            // {
            // for ( Constructor<?> con: constrs )
            // {
            // if ( con.getParameterCount() == )
            // }
            // }
            // }
            // catch (ClassNotFoundException ex) // fallback to base Issue if the
            // // user class is not loaded (jar not
            // // available on receiver side)
            // {
            // System.out.println("Issue class " + classname +
            // " not found in Java, creating issue of base class ers.Issue") ;
            // }
            // catch( Exception ex ) {
            // System.out.println("Some other exception from `Class.forName(classname).asSubclass(Issue.class);`"
            // + "\nMessage: " + ex.getMessage() + "\nCause: " + ex.getCause());
            // }
            //
            // final Object[] initargs = { message };
            // Class<?>[] params = new Class[initargs.length];
            // int i = 0;
            // for (Object a : initargs)
            // params[i++] = a.getClass();
    
            // try {
            // final Constructor<? extends Issue> constr = issue_class.getConstructor(params);
    
            // an_issue = constr.newInstance(initargs);
    
            an_issue = new Issue(message); // for the moment create instance of base class
            an_issue.setContext(context);
            an_issue.setSeverity(new Severity(severity));
            an_issue.setValues(parameters);
            an_issue.setTime(time);
            an_issue.setQualifiers(qualifiers);
            an_issue.setCause(cause);
            an_issue.initCause(cause) ; 

            if(no_class) {
                an_issue.setId(issue_id);
            }
            
	    return an_issue;
            
            // } catch (java.lang.NoSuchMethodException ex) {
            // throw new BadIssue("Failed to get Constructor for issue of class "
            // + issue_class, ex);
            // } catch (InvocationTargetException ex) {
            // throw new BadIssue("Got exception in instantiation of " + issue_class
            // + ":" + ex.getTargetException().getMessage(),
            // ex.getTargetException());
            // } catch (java.lang.Exception ex) {
            // throw new BadIssue("Failed to create an instance of class "
            // + issue_class, ex);
            // }
	}

	/**
	 * Add a qualifier to the qualifier list. To be used by users building
	 * specific issues.
	 * 
	 * @param qualifier
	 *            the qualifier to add
	 */
	public final void addQualifier(final String qualifier) {
	    if (m_qualifiers.indexOf(qualifier) == -1) {
	        m_qualifiers.add(qualifier);
	    }
	}

	/** TODO
	 * NB: may be it's better to override Exception::getCause() (which returns null for normal Issue?)
	 * or call Throwable::initCause(Throwable cause) ?
	 *
	 * @return the cause Issue of this Issue
	 */
	public Issue cause() {
	    return m_cause;
	}

	/** @return the name of the Java class of this Issue */
	public String getClassName() {
	    return m_class.getName();
	}

	/** @return the ID if the issue, compatible with C++ namespace::class_name */
	public String getId() {
	    return m_id;
	}

	/**
	 * Private method, used only from createIssue
	 * 
	 * @param id
	 *            ID of the issue, compatible with C++ namespace::class_name
	 */
	private void setId(final String id) {
	    m_id = id;
	}

	/** @return Context of the issue */
	public AbstractContext context() {
	    return m_context;
	}

	/** @return Message, a general description of the issue */
	public String message() {
	    return m_message;
	}

	/** @return array of qualifiers */
	public List<String> qualifiers() {
	    return m_qualifiers;
	}

	/** @return map of parameters */
	public Map<String, String> parameters() {
	    return m_values;
	}

	/** @return Severity of the issue */
	public Severity severity() {
	    return m_severity;
	}

	/**
	 * @return String representation of time of the issue, in format Wed May 08
	 *         11:18:04 CEST 2013
	 */
	public String time() {
	    final Date time = new Date(m_time);
	    return time.toString();
	}
	
	/** @return int time in seconds since 1 Jan 1970 */
	public int timeT() {
	    // Calendar calendar = Calendar.getInstance();
	    // long utcTime = m_time + calendar.get(Calendar.ZONE_OFFSET) +
	    // calendar.get(Calendar.DST_OFFSET);
	    return (int) (m_time / 1000); // as m_time is in milliseconds and time_t
	                                  // is the number of seconds I divide by
	                                  // 1000
	}

	/** @return int time in MILLI-seconds since 1 Jan 1970 */
	public long ltime() {
	    return m_time;
	}

	/**
	 * This function sets the severity. Needed in reporting, where severity is
	 * actually redefined by the stream.
	 * 
	 * @param severity
	 *            the new severity
	 * @return the previous severity
	 */
	public Severity setSeverity(final Severity severity) {
	    final Severity old_severity = m_severity;
	    m_severity = severity;
	    return old_severity;
	}

	/**
	 * Adds parameter pair <name, value> to the issue. Template argument T must
	 * be convertible toString
	 * 
	 * @param key
	 *            name of parameter
	 * @param value
	 *            value of parameter
	 */
	public final <T> void setValue(final String key, final T value) {
	    m_values.put(key, value.toString());
	}

	private void setTime(final long time) {
	    m_time = time;
	}

	/**
	 * Replaces all parameters of the issue
	 * 
	 * @param values
	 *            new map of <name, value> pairs
	 */
	public void setValues(final Map<String, String> values) {
	    m_values = values;
	}

	public final void setContext(final AbstractContext context) {
	    m_context = context;
	}

	/**
	 * Replaces all parameters of the issue
	 * 
	 * @param qualifiers
	 *            new map of <name, value> pairs
	 */
	public void setQualifiers(final List<String> qualifiers) {
	    m_qualifiers = qualifiers;
	}

	private void setCause(final Issue cause) {
	    m_cause = cause;
	}

	/**
	 * this function gets the default qualifiers read from the environment
	 * variable TDAQ_ERS_QUALIFIERS
	 * 
	 * @param qualifiers
	 * @return
	 */
        private final void addDefaultQualifiers() {
            final String environment = java.lang.System.getenv("TDAQ_ERS_QUALIFIERS");
            if(environment != null) {
                final StringTokenizer stok = new StringTokenizer(environment, ",");
                while(stok.hasMoreTokens()) {
                    m_qualifiers.add(stok.nextToken());
                }
            }
        }

	/**
	 * this Builds LocalContext of the issue by getting all info from a stack
	 * trace.
	 * 
	 * @return LocalContext built Context
	 */								
        static LocalContext buildContext(final Throwable object) {
            final StackTraceElement[] stack = object.getStackTrace();
    
            // starting from Java 7 it's possible to make Throwables which has no stack trace -> emtpy array returned
            if(stack.length > 0) {
                StackTraceElement sel = stack[0];
                for(StackTraceElement selit : stack) {
                    if(!selit.getMethodName().equals("<init>") && !selit.getClassName().startsWith("ers.")
                       && !selit.getClassName().startsWith("mts."))
                    {
                        sel = selit;
                        break;
                    }
                }
                return new LocalContext(sel.getClassName(), sel.getFileName(), sel.getLineNumber(), sel.getMethodName());
            } else return new LocalContext("NOCONTEXT", "NOCONTEXT", 0, "NOCONTEXT");
        }
        
        // Utility function to format a string a-la printf
        private final static String formatMessage(final String format_message, final Object... params) {
            try {
                return String.format(format_message, params);
            }
            catch(final IllegalFormatException ex) {
                ex.printStackTrace();
                return "Illegally formatted message";
            }
        }

	private List<String> m_qualifiers;
	/** List of associated qualifiers */
	private Severity m_severity;
	/** Issue's severity */
	private String m_message;
	/** Issue's explanation text */
	private Map<String, String> m_values;
	/** List of user defined attributes. */
	private AbstractContext m_context;
	/** Context of the current issue */
	private Issue m_cause;
	/** Issue that caused the current issue */
	private long m_time;
	/** Time when issue was thrown */
	// time in UTC <-- number of milliseconds since January 1, 1970, 00:00:00 GMT
	private Class<?> m_class;
	/** Java class of the issue */
	private String m_id;
	/** ID of the issue, namespace::class_name */
	private List<String> m_params_names;

}
