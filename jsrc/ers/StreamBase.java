/** @file  Base interface for Input and Output ERS streams
 * @author Andrei Kazarov
 * @version 1.0
 * @brief ERS StreamBase interface.
 */

package ers;

/** defines common interface to be implemented by ERS Input and Output streams
*/
public interface StreamBase
{
/** to be called when program exits, place your stream destruction and resource deallocation */
void cleanUp() ; 		
}
