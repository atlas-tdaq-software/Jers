/*
 *  StreamFactory.java
 *  
 *  Created by Andrei Kazarov
 *  Copyright 2008 CERN. All rights reserved.
 *
 */

/** \file  OutputStream.java Defines base class for all jers in and out streams
 * Automatically register in StreamManager all classes inherited from StreamFactory.
 * \author Andrei Kazarov
 * \version 1.0
 * \brief ERS Output Issue stream interface.
 */

package ers;

import java.util.HashMap;
import java.util.Map;
import java.lang.reflect.* ;

public final class StreamFactory
{
// hash of stream keys to implementing class names
	private transient final Map<String, Class<? extends AbstractOutputStream>>		registered_out_streams;
	private transient final Map<String, Class<? extends AbstractInputStream>>		registered_in_streams;

	private static volatile StreamFactory INSTANCE = null;

	synchronized public static StreamFactory instance()
	{
		if (INSTANCE == null) {
			INSTANCE = new StreamFactory();
		}
		return INSTANCE;
	}

	private StreamFactory()
		{
		registered_in_streams = 	new HashMap<String, Class<? extends AbstractInputStream>>() ;
		registered_out_streams = 	new HashMap<String, Class<? extends AbstractOutputStream>>() ;
		}

public void registerStream(final Class<? extends StreamBase> aclass) throws BadStream { 	
	try	{
		final String name = aclass.getAnnotation(ers.ErsStreamName.class).name() ;

		try {
			final Class<? extends AbstractInputStream> in_stream = aclass.asSubclass(AbstractInputStream.class) ;
			if ( registered_in_streams.containsKey(name) )
				{ throw new BadStream("Stream for key " + name + " is already registered by class " + in_stream.getName()) ; }
			registered_in_streams.put(name, in_stream) ;			
		} catch ( final ClassCastException ex ) { // not an InputStream 
			try	{
				final Class<? extends AbstractOutputStream> out_stream = aclass.asSubclass(AbstractOutputStream.class) ;
				if ( registered_out_streams.containsKey(name) )
					{ throw new BadStream("Stream for key " + name + " is already registered by class " + out_stream.getName()) ; }
				registered_out_streams.put(name, out_stream) ;
			} catch ( final ClassCastException exx ) { // not an OutputStream
				throw new BadStream("Failed to register stream " + aclass + ": it is not inherited from a proper Stream class", exx) ;
			}
		}
	} catch ( final Exception ex) {
		throw new BadStream("Failed to register stream " + aclass, ex) ;
	}
}

public  AbstractOutputStream createOutStream(final String[] stream_tokens) throws BadStreamConfiguration
	{
	final String key = stream_tokens[0] ;
	if ( registered_out_streams.get(key) == null )
		{ throw new BadStreamConfiguration("Stream for key " + key + " is not registered") ; }

	// prepare arguments for calling constructor
	Object[] initargs = new Object[stream_tokens.length - 1] ;
	for ( int i = 1 ;  i < stream_tokens.length ; i++ )
		{ initargs[i-1] = stream_tokens[i] ; }
		
	Class<?>[] params = new Class[initargs.length] ; int i = 0 ;
	for (Object a: initargs) { params[i++] = a.getClass() ; }

	// call registered stream constructor
	try 	{
		final Constructor<? extends AbstractOutputStream> couts = registered_out_streams.get(key).getConstructor(params) ;
		return couts.newInstance(initargs) ;
		}
	catch ( java.lang.NoSuchMethodException ex ) 
		{ throw new BadStreamConfiguration("Failed to get Constructor for class " + registered_out_streams.get(key), ex) ; }
	catch ( InvocationTargetException ex )
		{ throw new BadStreamConfiguration("Got exception in instantiation of " + registered_out_streams.get(key) + ":" + ex.getCause(), ex.getCause()) ; }
	catch ( java.lang.Exception ex )
		{ throw new BadStreamConfiguration("Failed to create an instance for class " + registered_out_streams.get(key), ex) ; }
	}

//public 	AbstractInputStream createInStream(final String key, final String options) throws BadStreamConfiguration
//	{
//	if ( registered_in_streams.get(key) == null )
//		{ throw new BadStreamConfiguration("Input stream for key " + key + " is not registered") ; }
//
//	// prepare arguments for calling constructor
//	Object[] initargs = new Object[1] ;
//	initargs[0] = options ;
//		
//	Class<?>[] params = new Class[1] ;
//	params[0] = options.getClass() ;
//
//	// call constructor
//	try 	{
//		final Constructor<? extends AbstractInputStream> cins = registered_in_streams.get(key).getConstructor(params) ;
//		return (AbstractInputStream)cins.newInstance(initargs) ;	
//		}
//	catch ( java.lang.NoSuchMethodException ex ) 
//		{ throw new BadStreamConfiguration("Failed to get Constructor with parameters " + initargs[0] + " for class " + registered_in_streams.get(key), ex) ; }
//	catch ( InvocationTargetException ex )
//		{ throw new BadStreamConfiguration("Got exception in instantiation of " + registered_in_streams.get(key) + ": " + ex.getTargetException().getMessage(), ex.getTargetException()) ; }
//	catch ( java.lang.Exception ex )
//		{ throw new BadStreamConfiguration("Failed to create an instance for " + registered_in_streams.get(key), ex) ; }
//	}

/**
 * Create stream by name, given variadic number of constructor parameters, to be called from 
 * StreamManager::add_receiver(final IssueReceiver receiver, final String stream, final Object... params)
 * @param key
 * @param params
 * @return
 * @throws BadStreamConfiguration
 */
public 	AbstractInputStream createInStream(final String key, final Object... params) throws BadStreamConfiguration
	{
	if ( !registered_in_streams.containsKey(key) )
		{ throw new BadStreamConfiguration("Input stream for key " + key + " is not registered") ; }

	Class<?>[] params_types = new Class[params.length] ;
	int i = 0 ;
	for ( Object param: params )
	    {	params_types[i++] = param.getClass() ; }

	// call constructor
	try 	{
		final Constructor<? extends AbstractInputStream> cins = registered_in_streams.get(key).getConstructor(params_types) ;
		return (AbstractInputStream)cins.newInstance(params) ;	
		}
	catch ( java.lang.NoSuchMethodException ex ) 
		{ throw new BadStreamConfiguration("Failed to get Constructor with parameters " + params + " for class " + registered_in_streams.get(key), ex) ; }
	catch ( InvocationTargetException ex )
		{
//		System.err.println("Target exception: " + ex.getCause() ) ;
//		System.err.println("Cause of Target exception: " + ex.getCause().getCause() ) ;
		throw new BadStreamConfiguration("Got exception in instantiation of " + registered_in_streams.get(key) + ": " + ex.getCause(), ex.getCause()) ; }
	catch ( java.lang.Exception ex )
		{ throw new BadStreamConfiguration("Failed to create an instance for " + registered_in_streams.get(key), ex) ; }
	}
}
