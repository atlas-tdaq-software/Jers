/*
 *  InputStream.java
 *  
 *  Created by Claire Biau on 02.06.08
 *  Copyright 2008 CERN. All rights reserved.
 *
 */

/** @file  InputStream.java Defines abstract interface for ERS input streams.
 * @author Andrei Kazarov
 */

package ers;

/**
 * ERS InputStream interface.
 * An ERS stream is a mean to send and receive issues.
 * InputStream defines receive(Issue) interface.
 * User of a concrete input stream implementation provides a IssueReceiver which is called back by the framework when an Issue is received.
 * Implementation classes must realize @ProviderFor(StreamRegistrator.class) interface (getStreamName method) in order to be used by the framework.
 * see mts/jsrc/mts/MTSInStream.java for example of defining an 'mts' InputStream and jers/examples/TestReceiver.java for example
 * of creating user IssueReceiver and using it with 'mts' input stream.
 */
public abstract class AbstractInputStream implements StreamBase {
	
	private transient IssueReceiver receiver ;

	/** Provide IssueReceiver which is called by the ERS framework when the new issue is received in a stream*/
	protected void setReceiver ( final IssueReceiver _receiver ) 
		{
		receiver = _receiver ;
		}

	/** Provide IssueReceiver which is called by the ERS framework when the new issue is received in a stream*/
	protected IssueReceiver getReceiver () 
		{
		return receiver ;
		}

	/** called by the implementation upon receiving a message*/
	public void receive( final Issue issue )
		{
		receiver.receive( issue ) ;
		}
}
