package ers.log4j;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;

import ers.Issue;

/**
 * This appender gets messages from log4j and translates them to JERS messages.
 * 
 * How to use: <br>
 * <ol>
 * <li>Get a log4j Logger as usual, i.e. org.apache.log4j.Logger log4jLogger = LogManager.getLogger("TestReporterLogger");</li>
 * <li>Supply the proper configuration, e.g. DOMConfigurator.configure("../examples/log4j_xmlConfig.xml");</li>
 * <li>Log an exception or message as per log4j logging, i.e. log4jLogger.error(ex); </li>
 * </ol>
 * 
 * <p> For a working example check
 * <a href="https://svnweb.cern.ch/cern/wsvn/atlastdaq/DAQ/online/Jers/trunk/examples/TestReporter.java"> examples/TestReporter.java</a>
 *  and <a href="https://svnweb.cern.ch/cern/wsvn/atlastdaq/DAQ/online/Jers/trunk/examples/log4j_xmlConfig.xml"> examples/log4j_xmlConfig.xml</a> of the Jers project
 * 
 * @authors lmagnoni, lpapaevgeniou
 * 
 */
public class JERSAppender extends AppenderSkeleton {


	/**
	 *  Creates and reports an ers.Issue from a log4j logged event.
	 *  Log4j allows user to log any Object.
	 * 
	 * @param event
	 *            event from log4j logger
	 */
	@Override
	protected void append(final LoggingEvent event) {
		Issue issue ;

		if (event.getMessage() instanceof ers.Issue) {
			// If the message of the event is an ers.Issue, then this means that
			// an Issue was passed to the log4j logger
			issue = (Issue) event.getMessage();
		} else if (event.getMessage() instanceof Exception) {
			// This means that an Exception is passed to log4j.
			//   So create an issue directly from the exception
			final Exception ex = (Exception) (event.getMessage());
			issue = new Issue( ex );
		}
		else {
			// Simplest case where a String was logged with log4j
			// if it is not an Exception it it expected to be a simple String
			assert (event.getMessage() instanceof String );
			issue = new Issue( (String) event.getMessage() );
		}

		
		// Here you can see the mapping of the corresponding levels/severities
		//   log4j levels in order: "TRACE", "DEBUG", "INFO",       "WARN", "ERROR", "FATAL"
		// ers severities in order: "DEBUG", "LOG", "INFORMATION", "WARNING", "ERROR", "FATAL"
	
		// However, setting the severity has no effect, since the corresponding 
		// stream will set it as needed.
		
		switch (event.getLevel().toInt()) {
			case (Level.TRACE_INT): {
				//issue.set_severity(new ers.Severity(ers.Severity.level.Debug));
				ers.Logger.debug(0, issue);
				break;
			}
			case (Level.DEBUG_INT): {
				//issue.set_severity(new ers.Severity(ers.Severity.level.Log));
				ers.Logger.log(issue);
				break;
			}
			case (Level.INFO_INT): {
				//issue.set_severity(new ers.Severity(ers.Severity.level.Information));
				ers.Logger.info(issue);
				break;
			}
			case (Level.WARN_INT): {
				//issue.set_severity(new ers.Severity(ers.Severity.level.Warning));
				ers.Logger.warning(issue);
				break;
			}
			case (Level.ERROR_INT): {
				//issue.set_severity(new ers.Severity(ers.Severity.level.Error));
				ers.Logger.error(issue);
				break;
			}
			case (Level.FATAL_INT): {
				//issue.set_severity(new ers.Severity(ers.Severity.level.Fatal));
				ers.Logger.fatal(issue);
				break;
			}
		}
	}

	@Override
	public void close() {
	}

	@Override
	public boolean requiresLayout() {
		return true;
	}
}
