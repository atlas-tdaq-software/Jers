/** @file  IssuePrinter.java 
 * @author Claire Biau, Andrei Kazarov
 */

package ers;

import java.text.DateFormat; 
import java.text.SimpleDateFormat; 
import java.text.FieldPosition ;

import java.io.OutputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.Map.Entry;


/** Defines static print and println functions for formatted output an issue to any java.io.OutputStream stream.
 * The amount of information (verbosity) is defined by current ERS verbosity level.
 */
public final class IssuePrinter {
	
	final static String timeformat_env = "TDAQ_ERS_TIMESTAMP_FORMAT" ;
	// Default ERS formatting + timezone
	static DateFormat date_format = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss.SSS z") ; 
	
	static {
		final String format = System.getenv(timeformat_env) ;
		if ( format != null ) {
			try {
				date_format = new SimpleDateFormat(format) ;
			} catch ( final java.lang.IllegalArgumentException ex ) {
				System.err.println("Invalid DateFormat specified in TDAQ_ERS_TIMESTAMP_FORMAT: " + format) ;
				ex.printStackTrace() ;
			}
		}
	} 
  
	private static String separator ;
 
	private IssuePrinter() {} ; // static class, no need in public constructor 
	
	/**
	 * This generic function prints an issue, depending on the defined verbosity
	 * Normally verbosity is taken from ers.Configuration 
	 * @param out a stream to print Issue to
	 * @param exception Exception or ers::Issue to print
	 * @param verbosity verbosity level, given by current ERS configuration
	 * @return the same OutputStream
	 */
	public static OutputStream print ( final OutputStream out, final Throwable exception, final Severity severity,
		final int verbosity, final boolean shift) throws java.io.IOException
	{
		if ( shift ) 	{ separator = "\n\t\t" ; }
		else		{ separator = "\n\t" ; }
		
		boolean 		is_issue = false ;
		Issue			issue = null ;
		final AbstractContext 	context ;
		
		if ( exception instanceof Issue )
		    { is_issue = true; issue = (Issue)exception ; context = issue.context() ; } 
		else
		    { context = Issue.buildContext(exception) ; }
		
		final StringBuffer stringTemp = new StringBuffer();
		if ( shift ) 	{ stringTemp.append("\t") ; } 
		
		if ( verbosity > -3 && !shift )
		{
			stringTemp.append(severity.toString()).append("\t");
		}
		
		if ( verbosity > -2 && !shift )
		{
			// stringTemp.append(new Date(context.time()).toString().concat(" "));
			date_format.format(new Date(context.time()), stringTemp, new FieldPosition(0)) ;
			stringTemp.append(" ") ;
		}
		
		if ( verbosity > -1 )
		{
			stringTemp.append('[').append(context.package_name()).append('@') ;
			stringTemp.append(context.function_name()).append(" at ") ;
			stringTemp.append(context.file_name()).append(':').append(context.line_number()).append("] ");
		}

/*		if ( verbosity > -1 )		// this is commented to be consistent with C++ issue printer
		{
			if( is_issue ) {
				stringTemp.append(' ').append(issue.id()).append(": ") ;
			}
		} */
		
		if ( is_issue ) stringTemp.append(issue.message()) ;
		else 		stringTemp.append(exception) ; 	// NB: this calls exception.toString which prints exception id + message
								// this is not fully compatible with C++ printing
		
		if ( verbosity > 1 && is_issue ) {
		     stringTemp.append(separator);
		     stringTemp.append("Parameters = ");
		     for  ( Iterator<Entry<String, String>> iterator = issue.parameters().entrySet (  ) .iterator (  ) ; iterator.hasNext (  ) ; )   
			 {  
			 final Entry<String, String> entry =  iterator.next (  ) ;
			 stringTemp.append('\'');
			 stringTemp.append((String) entry.getKey (  ));
			 stringTemp.append('=');
			 stringTemp.append((String) entry.getValue (  ));
			 stringTemp.append("' ");
			 }

		     stringTemp.append(separator);
		     stringTemp.append("Qualifiers = ");
		     for  ( String elt : issue.qualifiers() )   
			 {  				      
			 stringTemp.append('\'');
			 stringTemp.append(elt);
			 stringTemp.append("' ");
			 }
		}
		
		if ( verbosity > 2 )
		{
			stringTemp.append(separator);
			stringTemp.append("application = ");
		    	stringTemp.append(context.application_name());
			stringTemp.append(separator);
			stringTemp.append("host = ");
		    	stringTemp.append(context.host_name());
			stringTemp.append(separator);
			stringTemp.append("user = ");
		    	stringTemp.append(context.user_name());			
		    	stringTemp.append(" (");
		    	stringTemp.append(context.user_id());			
		    	stringTemp.append(')');		    
		   	stringTemp.append(separator);
			stringTemp.append("process id = ");
		    	stringTemp.append(context.process_id());			
			stringTemp.append(separator);
			stringTemp.append("thread id = ");
		    	stringTemp.append(context.thread_id());			
			stringTemp.append(separator);
			stringTemp.append("process wd = ");
		    	stringTemp.append(context.cwd());			
		}

		if ( verbosity > 3 )
		{
			stringTemp.append(separator);
			stringTemp.append("package name = ").append(context.package_name() );			
			stringTemp.append(separator);
			stringTemp.append("function name = ").append(context.function_name() );			
			stringTemp.append(separator);
			stringTemp.append("file name = ").append(context.file_name() );			
			stringTemp.append(separator);
			stringTemp.append("line number = ").append(context.line_number() );			
			
		}

		if ( verbosity > 4 )
		{
			final StackTraceElement[] stack = context.stack_symbols();
			int idx = 0;
			for  ( StackTraceElement elt : stack )   
			    {  			      
			    stringTemp.append(separator);
			    stringTemp.append('#');
			    stringTemp.append(idx);
			    if ( idx < 10 )
			    	stringTemp.append(' ');
			    if ( idx < 100) 
			    	stringTemp.append(' ');
			    
			    stringTemp.append(elt);
			    idx++;
			    } 
		}
				
		if ( (is_issue && issue.cause() != null) || (!is_issue && exception.getCause() != null) )
		{
			stringTemp.append("\n\twas caused by: " + 
				(is_issue? issue.cause().getId() : "" ) + "\n") ;
		}

		out.write( stringTemp.toString().getBytes() ) ;
		
		if ( is_issue && issue.cause() != null )
		    { print(out, issue.cause(), severity, verbosity, true) ; }
		else if ( exception.getCause() != null )
		    { print(out, exception.getCause(), severity, verbosity, true) ; }

		return out;
	}
	
	/**
	 * This function prints an issue
	 * According to the verbosity defined it will not print the same things
	 * @param out a stream to print Issue to
	 * @param issue Issue to print
	 * @param verbosity verbosity level, given by current ERS configuration
	 * @return the same OutputStream
	 */
	public static OutputStream println ( final OutputStream out, final Throwable exception, final Severity severity,
		final int verbosity) throws java.io.IOException
	{
		print ( out, exception, severity, verbosity, false );
		out.write('\n');
		// out.flush();
		return out;
	}
	
}
