package ers ;

public class BadIssue extends Exception
{
  
  private static final long serialVersionUID = 1L;

  public BadIssue()
  {
    	super() ;             
  }
 
  public BadIssue(final String err)
  {
    super(err) ;
  }

  public BadIssue(final String err, final Throwable cause)
  {
	super(err, cause) ; 
  }
}
  
