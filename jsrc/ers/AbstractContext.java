/** @file  Context.java This file defines the ers Context interface.
 * @author Claire Biau, Andrei Kazarov
 */

package ers;

/**
 * This class provides an abstract interface for the Context information of an issue.
 * The implementation might be different depending on whether the current context is
 * local. i.e. produced in the current process or it was received as part of the
 * issue, which has been created in another process.
 */
public abstract class AbstractContext {
	
	public abstract String cwd() ;           		/**@return current working directory of the process */
	public abstract String file_name() ;			/**@return name of the file which created the issue */
	public abstract String function_name() ;		/**@return name of the function which created the issue */
	public abstract String package_name() ;			/**@return Java package name */
	public abstract int line_number();			/**@return line number, in which the issue has been created */
	public abstract String host_name() ;    		/**@return host where the process is running */
	public abstract int process_id() ;           		/**@return process id */      
	public abstract long thread_id() ;			/**@return thread id */
	public abstract StackTraceElement[] stack_symbols() ;	/**@return stack frames */
	public abstract int stack_size() ; 			/**@return number of frames in stack */
	public abstract long user_id() ; 			/**@return user id */
	public abstract String user_name() ;			/**@return user name */
	public abstract String application_name() ;		/**@return application name: new since tdaq-common-01-18-03 */
	public abstract long time() ;				/**@return Time when issue was thrown */
}
