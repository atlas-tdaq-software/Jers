#!/usr/bin/env tdaq_python

import time
import os
import sys
from optparse import OptionParser
# Current user
def current_user():
    import pwd
    try:
        return pwd.getpwuid(os.getuid()).pw_name
    except KeyError:
        return "(unknown)"
    
def write_file(ClassName, listOfContent, extension):
    """This function write the content given in a file.The file name is composed of the ClassName and the extension. \
    If the file doesn't exist it is created. It alsways overwrites it."""
    file = open(ClassName+'.' + extension,'w')
    for line in listOfContent :
        file.write(line + '\n')

    file.close()

def create_java_file_lines(BaseClass, ClassName, PackageName, Message):
    """This function creates the content of the java file line by line"""
    lines = []
    actualTime  = time.strftime('%d/%m/%y %H:%M',time.localtime())

    #Create the header of the file
    lines.append('/* ' + ClassName + '.java')
    lines.append(' * Created by ' + current_user() + ' on ' + actualTime)
    lines.append(' * Copyright 2008 CERN. All rights reserved.')
    lines.append(' * This file is a user defined issue \n */\n')

    #Package name and imports
    lines.append('package ' + PackageName + ';\n')
    lines.append('import java.util.Date; \nimport java.util.HashMap; \nimport java.util.Map; \nimport java.util.StringTokenizer; \nimport java.util.Vector; \n\nimport ers.*;')
    

    if ((BaseClass=="null")or(BaseClass=="Null")or(BaseClass=="NULL")):
	extendsFrom = "ers.Issue"
    else:
	extendsFrom = BaseClass

    #Create class
    lines.append('\npublic class ' + ClassName + ' extends ' + extendsFrom + ' { \n\n\
    	public ' + ClassName + ' (' + ClassName + ' other)\n\
	{\n\
		super(other);\n\
		m_message = \"' + Message  + '\" + other.m_message;\n\
	}\n' )

    lines.append('	public ' + ClassName + '(Context context, String message )\n\
	{\n\
		super(context, message);\n\
		m_message = \"' + Message  + '\" + message;\n\
	}\n' )

    lines.append('	public ' + ClassName + '(String message )\n\
	{\n\
		super(message);\n\
		m_message = \"' + Message  + '\" + message;\n\
	}\n' )
    
    lines.append('	public ' + ClassName + '( Context context, Exception cause )\n\
	{\n\
		super(context, cause);\n\
		m_message = \"' + Message  + '\";\n\
	}\n')

    lines.append('	public ' + ClassName + '( Context context, String message, Exception cause )\n\
	{\n\
		super(context, message, cause);\n\
		m_message = \"' + Message  + '\" + message;\n\
	}\n')
    
    lines.append('}\n\n')
    
    return lines


def create_cplusplus_header_file_lines(BaseClass, ClassName, Namespace, Message):
    """This function creates the content of the c++ header file line by line"""
    
    if ((BaseClass=="null")or(BaseClass=="Null")or(BaseClass=="NULL")):
	BaseClass = ""
    
    lines = []

    lines.append('#ifndef ' + ClassName.upper() + '_H_INCLUDED')
    lines.append('#define ' + ClassName.upper() + '_H_INCLUDED\n')

    lines.append('#include "ers/ers.h"\n')


    lines.append('namespace daq {\n')

    lines.append('/******************************/')
    lines.append('/**  Applications Issues     **/')
    lines.append('/******************************/\n')

    lines.append('/**')
    lines.append(' * Application cannot be started')
    lines.append(' */')
    lines.append('  ERS_DECLARE_ISSUE_BASE( ' + Namespace + ',')
    lines.append('                          ' + ClassName + ',')
    lines.append('                          ' + BaseClass + ',')
    lines.append('                          \"' + Message + '\" << text,')
    lines.append('                         ,')
    lines.append('                         ((const char *)text)')
    lines.append('                       )')
    lines.append('} // daq namespace')
    lines.append('#endif')
    
    return lines

def mainFunction(BaseClass, ClassName, PackageName, Message):

    print ("BaseClass = " + BaseClass + " ClassName = " + ClassName + " PackageName = " + PackageName + " Message = " +Message)
          
    content_java = create_java_file_lines(BaseClass, ClassName,PackageName, Message)
    content_h = create_cplusplus_header_file_lines(BaseClass, ClassName, PackageName, Message)
    write_file(ClassName,content_java,'java')
    write_file(ClassName,content_h,'h')
        
if __name__ == '__main__':
    args = sys.argv[1:]
    
    if (args.__len__() < 2):
        print ('Please call the python script as follows : Create_user_issue_functions.py BaseClass ClassName PackageName [Message] \nThe BaseClass is optional, if you don\'t need it, write "null" instead')
    else:
        if (args.__len__() >= 3):
            argv = []
            argv.append(args[0])
            argv.append(args[1])
            argv.append(args[2])  
	    argv.append("")
	    if (args.__len__() > 3):
            	argv[3] += args[3]
		for i in range(4, len(args)):
                	argv[3] += " " + args[i]
	

        mainFunction(*argv)





#mainFunction('testBaseClass','testClassERS', 'ers', 'voicic le message')
#mainFunction('null','testClassERS2', 'ers', 'voicic le message')
