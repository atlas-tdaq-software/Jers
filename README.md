# Jers - Java ERS

A Java implementation of [ERS functionality](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/doxygen/tdaq-08-03-01/html/main.html).

## Jers how-to
https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/javadoc/tdaq-08-03-01/ers/package-summary.html

### Reporting messages
Reporting is done by creating custom [ERS Issues](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/javadoc/tdaq-08-03-01/index.html?ers/package-summary.html) and sending them to the [Logger class API](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/javadoc/tdaq-08-03-01/ers/Logger.html). See [MyTestIssue](examples/MyTestIssue.java) and [TestReporter](examples/TestReporter.java) examples.

See also related [MTS](https://gitlab.cern.ch/atlas-tdaq-software/mts) package which provides a transport layer for sending, subscribing and receiving ERS messages in distributed environment. 

### Reporting to ERS from Log4J
You can direct your standard log4j logs to ERS using [configuration](examples/log4j_xmlConfig.xml), as described in
https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/javadoc/tdaq-08-03-01/index.html?ers/package-summary.html
