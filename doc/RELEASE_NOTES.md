# Jers - Java ERS

## tdaq-11-04-00

Use of unmaintained SPI (https://github.com/rspilker/spi) for annotating classes providing ers.Stream interface for runtime registration in ERS StreamFactory is replaced with org.reflections (https://github.com/ronmamo/reflections). Now stream classes are declared (annotated) like

```java
@ErsStreamName(name="null")
public class NullStream extends AbstractOutputStream { ...
```

The list of packages for search of stream providers is `ers` and `mts`.

## tdaq-09-00-01

Use of `TDAQ_ERS_TIMESTAMP_FORMAT` with default value `"yyyy-MMM-dd HH:mm:ss,SSS z"` to allow defining timestamp format like in ERS, including milliseconds.
The format used before was `"DDD MMM dd HH:mm:ss z yyyy"` e.g. `Mon Mar 30 15:06:28 CEST 2020`.

## tdaq-09-00-00
